using System;
using UnityEngine;

namespace PTWO_PR
{
    public class AnimationSpeedHandler : MonoBehaviour
    {
        private Animator anim;

        private void Start()
        {
            anim = GetComponent<Animator>();
            anim.speed = anim.speed * GameManager.instance.GlobalAnimationSpeed;
        }
    }
}