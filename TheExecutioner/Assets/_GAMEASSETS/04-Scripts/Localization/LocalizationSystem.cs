﻿using PTWO_PR.Events;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

namespace PTWO_PR
{
    public class LocalizationSystem : MonoBehaviour
    {
        
        private void OnEnable()
        {
            LocalizationSettings.SelectedLocaleChanged += OnLanguageChange;
        }
        private void OnDisable()
        {
            LocalizationSettings.SelectedLocaleChanged -= OnLanguageChange;
        }

        private void OnLanguageChange(Locale locale)
        {
            Message.Raise(new LanguageChangeEvent(locale, locale.Identifier.ToString()));
        }
    }
}