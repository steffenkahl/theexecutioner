using PTWO_PR.Events;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Tables;

namespace PTWO_PR
{
    public class LocalizeString : MonoBehaviour
    {
        [SerializeField] private LocalizedString localizedString;
        private string localizedText;
        private TMP_Text textField;
        private StringTable languageTable;

        private void OnEnable()
        {
            Message<LanguageChangeEvent>.Add(OnLanguageChange);
            ShowString();
        }

        private void OnDisable()
        {
            Message<LanguageChangeEvent>.Remove(OnLanguageChange);
            localizedString.StringChanged -= UpdateString;
        }

        private void Awake()
        {
            textField = GetComponent<TMP_Text>();
        }


        private void ShowString()
        {
            if (!textField)
            {
                return;
            }

            textField.text = localizedString.GetLocalizedString();
        }

        public void ChangeText(LocalizedString newText)
        {
            localizedString = newText;
            ShowString();
        }

        private void OnLanguageChange(LanguageChangeEvent ctx)
        {
            ShowString();
        }
        
        void UpdateString(string s)
        {
            localizedText = s;
        }
    }
}