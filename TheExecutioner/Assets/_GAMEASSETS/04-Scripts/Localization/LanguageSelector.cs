﻿using System;
using System.Collections;
using System.Collections.Generic;
using PTWO_PR.Events;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

namespace PTWO_PR
{
    public class LanguageSelector : MonoBehaviour
    {
        private TMP_Dropdown dropdownSelector;

        private void OnEnable()
        {
            Message<LanguageChangeEvent>.Add(OnLanguageChanged);
        }

        private void OnDisable()
        {
            Message<LanguageChangeEvent>.Remove(OnLanguageChanged);
        }

        private void Start()
        {
            dropdownSelector = GetComponent<TMP_Dropdown>();

            StartCoroutine(LoadLanguages(1)); //Load the language selector options delayed, so the list is not empty
        }

        public void OnSelectionChanged()
        {
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[dropdownSelector.value];
        }

        private void OnLanguageChanged(LanguageChangeEvent ctx)
        {
            UpdateSelectedLanguage();
        }

        private void UpdateSelectedLanguage()
        {
            for(int i = 0; i < dropdownSelector.options.Count; i++)
            {
                if (dropdownSelector.options[i].text == LocalizationSettings.SelectedLocale.LocaleName)
                {
                    dropdownSelector.value = i;
                }
            }
        }
        
        IEnumerator LoadLanguages(float delayTime)
        {
            //Wait for the specified delay time before continuing.
            yield return new WaitForSeconds(delayTime);
 
            //Do the action after the delay time has finished.
            dropdownSelector.options = new List<TMP_Dropdown.OptionData>(); //Clear the possible options
            
            List<Locale> localesList = LocalizationSettings.AvailableLocales.Locales;
            
            foreach (Locale loc in localesList)
            {
                dropdownSelector.options.Add(new TMP_Dropdown.OptionData(loc.LocaleName));
            }

            UpdateSelectedLanguage();
        }
    }
}