using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UI;

namespace PTWO_PR
{
    /// <summary>
    /// Script for the customisation of Achievement Popups
    /// </summary>
    public class AchievementPopup : MonoBehaviour
    {
        [SerializeField] private Image achievementImage; //A reference to the small achievement icon
        [SerializeField] private LocalizeString achievementTitle; //A reference to the LocalizeString for the title
        [SerializeField] private LocalizeString achievementDescription; //A reference to the LocalizeString for the description
        [SerializeField] private Animator animator; //A reference to the animator to animate a slide-in
        [SerializeField] private float yAnchor; //The height that is the anchor of new achievementPopups before altering it to fit
        private int childSiblings; //How many siblings a popup has
        private int childID; //what id of all the siblings, this achievement has
        private RectTransform rectTrans; //A reference to the RectTransform to set positions

        private void Start()
        {
            rectTrans = GetComponent<RectTransform>(); //Get a reference to the RectTransform of this Popup to make calculations from it
        }

        private void Update()
        {
            //Make sure to not overlay popups on top of each other
            if (transform.parent.childCount < childSiblings)
            {
                childSiblings -= 1;
                rectTrans.DOLocalMoveY(-yAnchor + (-rectTrans.rect.height * childID), 0.1f);
                childID -= 1;
            } else if (transform.parent.childCount > childSiblings)
            {
                childSiblings += 1;
            }
        }

        public void AfterAnimationDestroy()
        {
            Destroy(this.gameObject);
        }

        public void SetAchievement(Sprite achievementSprite, LocalizedString achievementTitleString, LocalizedString achievementDescriptionString, int pos)
        {
            //Calculate Y-Pos based on pos-Value to also make sure achievements dont overlap
            rectTrans = GetComponent<RectTransform>();
            childID = pos +1;
            rectTrans.DOLocalMoveY(-yAnchor + (-rectTrans.rect.height * childID), 0.1f); //Move the achievement up or down depending on the other achievement popups
            achievementImage.sprite = achievementSprite; //Sets the sprite of the achievement
            achievementTitle.ChangeText(achievementTitleString); //Sets the title of the achievement with the help of a localised string
            achievementDescription.ChangeText(achievementDescriptionString); //Sets the description of the achievement with the help of a localised string
            animator.SetTrigger("Show"); //Sets the animator up to slide the achievement in
            childSiblings = transform.parent.childCount; //count the siblings to allow rearranging of multiple achievement popups
        }
    }
}