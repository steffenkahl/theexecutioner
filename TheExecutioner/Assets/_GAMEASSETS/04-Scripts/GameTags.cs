namespace PTWO_PR
{
    public class GameTags
    {
        public static string ACHIEVEMENTPOPUP = "AchievementPopup";
        public static string MAGICMANAGER = "MagicManager";
        public static string STATISTICSMANAGER = "StatisticsManager";
        public static string ACHIEVEMENTMANAGER = "AchievementManager";
        public static string SAVEMANAGER = "SaveManager";
    }
}