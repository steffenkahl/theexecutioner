﻿namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called after a camera was changed
    /// </summary>
    public struct CameraChangedEvent
    {
        private Cameras newCamera;

        /// <summary>
        /// The new camera that should be active after a camera change
        /// </summary>
        public Cameras NewCamera => newCamera;

        public CameraChangedEvent (Cameras newCamera)
        {
            this.newCamera = newCamera;
        }
    }
}