namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called whenever the player leaves a bad impression on the king
    /// </summary>
    public struct KingBadImpressionEvent
    {
        private int badImpressions;

        /// <summary>
        /// How many bad impressions a player 
        /// </summary>
        public int BadImpressions => badImpressions;
        
        public KingBadImpressionEvent(int badImpressions)
        {
            this.badImpressions = badImpressions;
        }
    }
}