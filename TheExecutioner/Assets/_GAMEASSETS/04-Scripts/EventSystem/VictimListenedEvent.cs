namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called whenever a victim was listened to
    /// </summary>
    public struct VictimListenedEvent
    {
    }
}