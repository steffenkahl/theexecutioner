namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called after a failure was activated
    /// </summary>
    public struct FailureActivatedEvent
    {
        private Outfit lastHat;
        
        /// <summary>
        /// The last hat that the executioner wore
        /// </summary>
        public Outfit LastHat => lastHat;
        
        public FailureActivatedEvent(Outfit lastHat)
        {
            this.lastHat = lastHat;
        }
    }
}