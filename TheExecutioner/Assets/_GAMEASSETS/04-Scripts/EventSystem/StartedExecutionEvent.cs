namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called whenever an execution was started
    /// </summary>
    public struct StartedExecutionEvent
    {
    }
}