namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called whenever a new victim was generated/loaded
    /// </summary>
    public struct NewVictimLoadedEvent
    {
        private Peeple selectedPeep;
        private Dialogue selectedDialogue;

        /// <summary>
        /// The new selected dialogue of the new victim that's active
        /// </summary>
        public Dialogue SelectedDialogue => selectedDialogue;
        
        /// <summary>
        /// The new selected peep of the new victim that's active
        /// </summary>
        public Peeple SelectedPeep => selectedPeep;
        
        public NewVictimLoadedEvent(Peeple selectedPeep, Dialogue selectedDialogue)
        {
            this.selectedPeep = selectedPeep;
            this.selectedDialogue = selectedDialogue;
        }
    }
}