﻿using UnityEngine.Localization;

namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called when the language of the game got changed
    /// </summary>
    public struct LanguageChangeEvent
    {
        private Locale newLocale;
        private string localeIdentifier;

        /// <summary>
        /// The new locale that should be active after this event
        /// </summary>
        public Locale NewLocale => newLocale;

        /// <summary>
        /// A string as a language identifier for the new language
        /// </summary>
        public string LocaleIdentifier => localeIdentifier;

        public LanguageChangeEvent(Locale newLocale, string localeIdentifier)
        {
            this.newLocale = newLocale;
            this.localeIdentifier = localeIdentifier;
        }
    }
}