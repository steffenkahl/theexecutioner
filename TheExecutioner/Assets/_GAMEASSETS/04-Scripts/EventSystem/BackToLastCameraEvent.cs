﻿namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called when the last camera should get active
    /// </summary>
    public struct BackToLastCameraEvent
    {
    }
}