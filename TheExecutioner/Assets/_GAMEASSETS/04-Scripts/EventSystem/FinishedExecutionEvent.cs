namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called if an execution was finished
    /// </summary>
    public struct FinishedExecutionEvent
    {
        private int distanceToExcellentChoice;

        private int earnedCoins;

        /// <summary>
        /// The distance in points to a perfect stat-score
        /// </summary>
        public int DistanceToExcellentChoice => distanceToExcellentChoice;

        /// <summary>
        /// The amount of earned coins from Execution
        /// </summary>
        public int EarnedCoins => earnedCoins;
        
        public FinishedExecutionEvent(int distanceToExcellentChoice, int earnedCoins)
        {
            this.distanceToExcellentChoice = distanceToExcellentChoice;
            this.earnedCoins = earnedCoins;
        }
    }
}