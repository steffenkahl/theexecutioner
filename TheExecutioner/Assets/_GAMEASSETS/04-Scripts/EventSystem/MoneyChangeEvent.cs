﻿namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called whenever the money should be changed
    /// </summary>
    public struct MoneyChangeEvent
    {
        private int amount;

        /// <summary>
        /// The amount that gets added to the money (can also be negative)
        /// </summary>
        public int Amount => amount;

        public MoneyChangeEvent(int amount)
        {
            this.amount = amount;
        }
    }
}