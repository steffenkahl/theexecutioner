﻿namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called if the active camera should change
    /// </summary>
    public struct ChangeCameraEvent
    {
        private Cameras newCamera;

        /// <summary>
        /// The new camera to switch to
        /// </summary>
        public Cameras NewCamera => newCamera;

        public ChangeCameraEvent (Cameras newCamera)
        {
            this.newCamera = newCamera;
        }
    }
}