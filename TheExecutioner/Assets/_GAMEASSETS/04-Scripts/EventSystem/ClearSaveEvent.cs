﻿namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called if the save should get cleared
    /// </summary>
    public struct ClearSaveEvent
    {
    }
}