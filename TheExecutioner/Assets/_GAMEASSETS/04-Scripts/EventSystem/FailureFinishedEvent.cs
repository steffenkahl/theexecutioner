namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called after a failure was finished (executed the last executioner)
    /// </summary>
    public struct FailureFinishedEvent
    {
    }
}