namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called whenever the currently selected weapon changed
    /// </summary>
    public struct SelectedWeaponEvent
    {
        private Weapon selectedWeapon;

        /// <summary>
        /// The new weapon that got selected
        /// </summary>
        public Weapon SelectedWeapon => selectedWeapon;
        
        public SelectedWeaponEvent(Weapon selectedWeapon)
        {
            this.selectedWeapon = selectedWeapon;
        }
    }
}