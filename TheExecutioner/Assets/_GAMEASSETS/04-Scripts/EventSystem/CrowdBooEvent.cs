namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called if the crowd should boo
    /// </summary>
    public struct CrowdBooEvent
    {
        private int plausabilityToBoo;

        /// <summary>
        /// The plausability in percent if a crowdpeep should boo
        /// </summary>
        public int PlausabilityToBoo => plausabilityToBoo;

        public CrowdBooEvent(int plausabilityToBoo)
        {
            this.plausabilityToBoo = plausabilityToBoo;
        }
    }
}