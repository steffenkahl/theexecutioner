namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called if the crowd should cheer
    /// </summary>
    public struct CrowdCheerEvent
    {
        private int plausabilityToCheer;

        /// <summary>
        /// The plausability in percent if a crowdpeep should cheer
        /// </summary>
        public int PlausabilityToCheer => plausabilityToCheer;

        public CrowdCheerEvent(int plausabilityToCheer)
        {
            this.plausabilityToCheer = plausabilityToCheer;
        }
    }
}