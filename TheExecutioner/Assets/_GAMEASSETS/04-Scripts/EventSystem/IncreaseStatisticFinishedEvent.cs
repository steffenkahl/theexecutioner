using System.Collections.Generic;

namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called if a statistic was increased
    /// </summary>
    public struct IncreaseStatisticFinishedEvent
    {
        private Dictionary<Statistic, int> statisticDict;
        private Statistic statisticToIncrease;
        private int amount;

        /// <summary>
        /// The complete dictionary of all statistics
        /// </summary>
        public Dictionary<Statistic, int> StatisticDict => statisticDict;
        
        /// <summary>
        /// The statistic which was increased
        /// </summary>
        public Statistic StatisticToIncrease => statisticToIncrease;
        
        /// <summary>
        /// The amount how much a statistic was increased
        /// </summary>
        public int Amount => amount;
        

        public IncreaseStatisticFinishedEvent(Statistic statisticToIncrease, int amount, Dictionary<Statistic, int> statisticDict)
        {
            this.statisticToIncrease = statisticToIncrease;
            this.amount = amount;
            this.statisticDict = statisticDict;
        }
    }
}