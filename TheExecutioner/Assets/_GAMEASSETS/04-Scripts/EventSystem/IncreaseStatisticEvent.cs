namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called whenever a statistic should increase
    /// </summary>
    public struct IncreaseStatisticEvent
    {
        private Statistic statisticToIncrease;
        private int amount;

        /// <summary>
        /// The statistic which should get increased
        /// </summary>
        public Statistic StatisticToIncrease => statisticToIncrease;
        
        /// <summary>
        /// The amount which should be added to the selected statistic
        /// </summary>
        public int Amount => amount;

        public IncreaseStatisticEvent(Statistic statisticToIncrease, int amount)
        {
            this.statisticToIncrease = statisticToIncrease;
            this.amount = amount;
        }
    }
}