﻿namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called whenever the game was loaded
    /// </summary>
    public struct SaveLoadedEvent
    {
        private SaveManager activeSaveManager;

        /// <summary>
        /// A reference of the active save manager to make connections easier
        /// </summary>
        public SaveManager ActiveSaveManager => activeSaveManager;

        public SaveLoadedEvent(SaveManager activeSaveManager)
        {
            this.activeSaveManager = activeSaveManager;
        }
    }
}