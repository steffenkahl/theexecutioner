namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called if a pardon was finished
    /// </summary>
    public struct FinishedPardonEvent
    {
        private int earnedCoins;

        /// <summary>
        /// The amount of earned coins from Pardon
        /// </summary>
        public int EarnedCoins => earnedCoins;
        
        public FinishedPardonEvent(int earnedCoins)
        {
            this.earnedCoins = earnedCoins;
        }
    }
}