namespace PTWO_PR.Events
{
    /// <summary>
    /// Gets called if the player earns an achievement
    /// </summary>
    public struct EarnAchievementEvent
    {
        private Achievement achievement;
        private bool silent;

        /// <summary>
        /// The achievement that should be earned
        /// </summary>
        public Achievement Achievement => achievement;
        
        /// <summary>
        /// If that achievement should be achieved in silence
        /// </summary>
        public bool Silent => silent;
        
        public EarnAchievementEvent (Achievement achievement, bool silent = false)
        {
            this.achievement = achievement;
            this.silent = silent;
        }
    }
}