using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

namespace PTWO_PR
{
    public class SetVolume : MonoBehaviour
    {
        [SerializeField] private Slider volumeSlider;

        [SerializeField] private TextMeshProUGUI textMeshVolume; //serialized to be able to drag and drop the Text in the unity inspector

        private void Start()
        {
            volumeSlider.value = SaveManager.Instance.MusicVolume;
            ShowSliderValueVolume();
        }

        public void SetVolumeLevel()
        {
            SaveManager.Instance.ChangeMusicVolume(volumeSlider.value);
            ShowSliderValueVolume();
        }

        public void ShowSliderValueVolume()
        {
            string sliderMessage = ((int)(volumeSlider.value * 100)).ToString() + "%"; //to show the float value in an int number
            textMeshVolume.text = sliderMessage; //to Display the Message in Text
        }
    }
}