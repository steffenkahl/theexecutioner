using System;
using System.Collections;
using System.Collections.Generic;
using PTWO_PR;
using PTWO_PR.Events;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;
using UnityEngine.Localization.SmartFormat.PersistentVariables;

namespace PTWO_PR
{
    public class FailureManager : MonoBehaviour
    {
        [SerializeField] private GameObject dungeonCanvas;
        [SerializeField] private GameObject failureCanvas;
        [SerializeField] private GameObject victim;
        [SerializeField] private GameObject failedExecutioner;
        [SerializeField] private TMP_Text failedExecutionerDescription;
        [SerializeField] private LocalizedString failureText;
        [SerializeField] private GameObject overlayUI;
        [SerializeField] private Weapon failedExecutionerWeapon;

        [SerializeField] private bool isInFailureMode;

        private bool weaponsCleared = true;
        
        public int victimAmount = 0;
        public int moneyAmount = 0;
        public int weaponAmount = 0;

        public bool IsInFailureMode => isInFailureMode;

        private static FailureManager instance;
        public static FailureManager Instance => instance;

        private void Awake()
        {
            instance = this;
        }

        private void OnEnable()
        {
            Message<FailureEvent>.Add(OnFailureEvent);
            Message<FinishedExecutionEvent>.Add(OnFinishedExecutionEvent);
            Message<VictimListenedEvent>.Add(OnVictimListenedEvent);
        }

        private void OnDisable()
        {
            Message<FailureEvent>.Remove(OnFailureEvent);
            Message<FinishedExecutionEvent>.Remove(OnFinishedExecutionEvent);
            Message<VictimListenedEvent>.Remove(OnVictimListenedEvent);
        }

        private void OnFailureEvent(FailureEvent ctx)
        {
            weaponsCleared = false;
            failureText.Clear();
            isInFailureMode = true;
            victimAmount = StatisticsManager.Instance.GetStatistic(Statistic.VICTIMSKILLED_LIFE);
            moneyAmount = StatisticsManager.Instance.GetStatistic(Statistic.MONEYEARNED_LIFE);
            weaponAmount = StatisticsManager.Instance.GetStatistic(Statistic.WEAPONSBOUGHT_LIFE);

            dungeonCanvas.SetActive(false);
            failureCanvas.SetActive(true);
            victim.SetActive(false);
            failedExecutioner.SetActive(true);

            failureText.Add("moneyAmount", new IntVariable {Value = moneyAmount});
            failureText.Add("victimAmount", new IntVariable {Value = victimAmount});
            failureText.Add("weaponAmount", new IntVariable {Value = weaponAmount});
            failedExecutionerDescription.text = failureText.GetLocalizedString();

            overlayUI.SetActive(false);
            
            Message.Raise(new IncreaseStatisticEvent(Statistic.FAILURES, 1));
            Message.Raise(new FailureActivatedEvent(OutfitManager.Instance.ChosenHat));
            Message.Raise(new OutfitChangedEvent());
        }

        private void OnFinishedExecutionEvent(FinishedExecutionEvent ctx)
        {
            if (isInFailureMode)
            {
                isInFailureMode = false;
                overlayUI.SetActive(true);
                dungeonCanvas.SetActive(true);
                failureCanvas.SetActive(false);
                victim.SetActive(true);
                failedExecutioner.SetActive(false);
                Message.Raise(new FailureFinishedEvent());
            }
        }

        public void OnExecuteFailedExecutioner()
        {
            GameManager.instance.ActivateButton();
            Message.Raise(new SelectedWeaponEvent(failedExecutionerWeapon));
            Message.Raise(new ChangeCameraEvent(Cameras.MAINSTAGE));
        }

        private void OnVictimListenedEvent(VictimListenedEvent ctx)
        {
            if (weaponsCleared == false)
            {
                Message.Raise(new AfterFailureEvent());
                weaponsCleared = true;
            }
        }
    }
}