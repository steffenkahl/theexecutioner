using System.Collections.Generic;
using DG.Tweening;
using PTWO_PR.Events;
using UnityEditor;
using UnityEngine;

namespace PTWO_PR
{
    public class CrowdManager : MonoBehaviour
    {
        [Header("MainStageBarriers")]
        [SerializeField] private GameObject mainStageBarrierMin;
        [SerializeField] private GameObject mainStageBarrierMax;
        
        [Header("RiverStageBarriers")]
        [SerializeField] private GameObject riverStageBarrierMin;
        [SerializeField] private GameObject riverStageBarrierMax;

        [Header("Perfect Scores")]
        [SerializeField] private int amountOfPerfectScores;
        [SerializeField] private int maxDistanceToBePerfect;
        [SerializeField] private int perfectScoreAmountForEvent;

        [Header("Coin Throw")] 
        [SerializeField] private GameObject CoinPrefab;
        [SerializeField] private Transform coinTargetPointMain;
        [SerializeField] private Transform coinTargetPointRiver;
        [SerializeField] private float coinVariationFromPoint;
        [SerializeField] private float coinFlySpeed;
        
        [Header("Other")] 
        [SerializeField] private GameObject crowdPeepPrefab;
        [SerializeField] private List<CrowdOutfit> outfits;
        [SerializeField] private float crowdSatisfaction;
        [SerializeField] private float crowdMultiplier;
        [SerializeField] private List<GameObject> crowdPeepsMain = new List<GameObject>();
        [SerializeField] private List<GameObject> crowdPeepsRiver = new List<GameObject>();
        [SerializeField] private AnimationCurve satisfactionChangePerDistance;
        [SerializeField] private AnimationCurve cheerPlausabilityPerDistance;
        [SerializeField] private float newPeepSpawnDistance;

        [SerializeField] private float minDistanceBetweenPeeps;
        
        float mainMinX;
        float mainMaxX;
        float mainMinY;
        float mainMaxY;
            
        float riverMinX;
        float riverMaxX;
        float riverMinY;
        float riverMaxY;
        
        
        private static CrowdManager instance;

        public static CrowdManager Instance => instance;

        private void OnEnable()
        {
            Message<FinishedExecutionEvent>.Add(OnFinishedExecutionEvent);
            Message<FinishedPardonEvent>.Add(OnFinishedPardonEvent);
        }
        
        private void OnDisable()
        {
            Message<FinishedExecutionEvent>.Remove(OnFinishedExecutionEvent);
            Message<FinishedPardonEvent>.Remove(OnFinishedPardonEvent);
        }
        
        private void OnValidate()
        {
            PopulateList();
        }

        private void Start()
        {
            instance = this;
            
            mainMinX = mainStageBarrierMin.transform.position.x;
            mainMaxX = mainStageBarrierMax.transform.position.x;
            mainMinY = mainStageBarrierMin.transform.position.y;
            mainMaxY = mainStageBarrierMax.transform.position.y;
            
            riverMinX = riverStageBarrierMin.transform.position.x;
            riverMaxX = riverStageBarrierMax.transform.position.x;
            riverMinY = riverStageBarrierMin.transform.position.y;
            riverMaxY = riverStageBarrierMax.transform.position.y;

            for (int i = 0; i < crowdSatisfaction * crowdMultiplier; i++)
            {
                SpawnCrowdPeep();
            }
        }
        
        private void PopulateList()
        {
            outfits = new List<CrowdOutfit>();
            
            #if UNITY_EDITOR
            string[] assetNamesPeeple = AssetDatabase.FindAssets("t:ScriptableObject",
                new[] {"Assets/_GAMEASSETS/06-Objects/CrowdOutfits"});
            foreach (string sOName in assetNamesPeeple)
            {
                string sOpath = AssetDatabase.GUIDToAssetPath(sOName);
                CrowdOutfit crowdOutfit = AssetDatabase.LoadAssetAtPath<CrowdOutfit>(sOpath);
                outfits.Add(crowdOutfit);
            }
            #endif
        }

        public CrowdOutfit GetRandomClothes(ClothPlaces place)
        {
            List<CrowdOutfit> possibleCloth = new List<CrowdOutfit>();
            foreach (CrowdOutfit co in outfits)
            {
                if (co.ClothPlace == place)
                {
                    possibleCloth.Add(co);
                }
            }

            return possibleCloth[Random.Range(0, possibleCloth.Count)];
        }

        private void OnFinishedPardonEvent(FinishedPardonEvent ctx)
        {
            //Calculate Money throw
            if (WeaponManager.Instance.ChosenWeapon.UsedScene == Cameras.MAINSTAGE)
            {
                for (int i = 0; i < ctx.EarnedCoins; i++)
                {
                    float xPosition = Random.Range(mainMinX, mainMaxX);
                    float yPosition = Random.Range(mainMinY, mainMaxY);
                    GameObject newCoin = GameObject.Instantiate(CoinPrefab, new Vector3(xPosition, yPosition),
                        Quaternion.Euler(0, 0, 0));
                    newCoin.transform.DOMove(coinTargetPointMain.position + Vector3.right * 
                        Random.Range(-coinVariationFromPoint, coinVariationFromPoint), coinFlySpeed).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        Destroy(newCoin);
                    });
                }
            }else if (WeaponManager.Instance.ChosenWeapon.UsedScene == Cameras.RIVER)
            {
                for (int i = 0; i < ctx.EarnedCoins; i++)
                {
                    float xPositionRiver = Random.Range(riverMinX, riverMaxX);
                    float yPositionRiver = Random.Range(riverMinY, riverMaxY);
                    GameObject newCoin = GameObject.Instantiate(CoinPrefab, new Vector3(xPositionRiver, yPositionRiver),
                        Quaternion.Euler(0, 0, 0));
                    newCoin.transform.DOMove(coinTargetPointRiver.position + Vector3.right * 
                        Random.Range(-coinVariationFromPoint, coinVariationFromPoint), coinFlySpeed).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        Destroy(newCoin);
                    });
                }
            }
        }
        
        private void OnFinishedExecutionEvent(FinishedExecutionEvent ctx)
        {
            crowdSatisfaction += satisfactionChangePerDistance.Evaluate(ctx.DistanceToExcellentChoice);

            if (cheerPlausabilityPerDistance.Evaluate(ctx.DistanceToExcellentChoice) > 0)
            {
                Message.Raise(new CrowdCheerEvent((int)cheerPlausabilityPerDistance.Evaluate(ctx.DistanceToExcellentChoice)));
            }
            else
            {
                Message.Raise(new CrowdBooEvent((int)cheerPlausabilityPerDistance.Evaluate(ctx.DistanceToExcellentChoice)));
            }

            if (ctx.DistanceToExcellentChoice < maxDistanceToBePerfect)
            {
                amountOfPerfectScores += 1;

                if (amountOfPerfectScores >= perfectScoreAmountForEvent)
                {
                    OnPerfectScores();
                }
            }
            
            //Calculate Money throw
            if (WeaponManager.Instance.ChosenWeapon.UsedScene == Cameras.MAINSTAGE)
            {
                for (int i = 0; i < ctx.EarnedCoins; i++)
                {
                    float xPosition = Random.Range(mainMinX, mainMaxX);
                    float yPosition = Random.Range(mainMinY, mainMaxY);
                    GameObject newCoin = GameObject.Instantiate(CoinPrefab, new Vector3(xPosition, yPosition),
                        Quaternion.Euler(0, 0, 0));
                    newCoin.transform.DOMove(coinTargetPointMain.position + Vector3.right * 
                        Random.Range(-coinVariationFromPoint, coinVariationFromPoint), coinFlySpeed).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        Destroy(newCoin);
                    });
                }
            }else if (WeaponManager.Instance.ChosenWeapon.UsedScene == Cameras.RIVER)
            {
                for (int i = 0; i < ctx.EarnedCoins; i++)
                {
                    float xPositionRiver = Random.Range(riverMinX, riverMaxX);
                    float yPositionRiver = Random.Range(riverMinY, riverMaxY);
                    GameObject newCoin = GameObject.Instantiate(CoinPrefab, new Vector3(xPositionRiver, yPositionRiver),
                        Quaternion.Euler(0, 0, 0));
                    newCoin.transform.DOMove(coinTargetPointRiver.position + Vector3.right * 
                        Random.Range(-coinVariationFromPoint, coinVariationFromPoint), coinFlySpeed).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        Destroy(newCoin);
                    });
                }
            }

            int maxExecution = 0;
            //Calculate The Crowd Peeple Amount:
            while ((int)(crowdSatisfaction * crowdMultiplier) > crowdPeepsMain.Count && maxExecution < 20)
            {
                SpawnCrowdPeep();
                maxExecution += 1;
            }

            maxExecution = 0; //Keeps while loops from running infinite
            
            while ((int)(crowdSatisfaction * crowdMultiplier) < crowdPeepsMain.Count && maxExecution < 20)
            {
                destroyCrowdPeep();
                maxExecution += 1;
            }
        }

        private void SpawnCrowdPeep()
        {
            
            //First create Peep on Main Stage:
            float xPosition = Random.Range(mainMinX, mainMaxX);
            float yPosition = Random.Range(mainMinY, mainMaxY);

            bool shouldRecalculate = true;
            int whileLimiter = 0;
            
            while (shouldRecalculate && whileLimiter < 200)
            {
                xPosition = Random.Range(mainMinX, mainMaxX);
                yPosition = Random.Range(mainMinY, mainMaxY);
                shouldRecalculate = false;
                foreach (GameObject go in crowdPeepsMain)
                {
                    float distance = Vector3.Distance(new Vector3(xPosition, yPosition),
                        new Vector3(go.transform.position.x, go.transform.position.y));
                    if (distance < minDistanceBetweenPeeps)
                    {
                        shouldRecalculate = true;
                    }
                }

                whileLimiter += 1;
            }

            Vector3 spawnPosition = new Vector3(xPosition - newPeepSpawnDistance, yPosition);
            GameObject newPeepMain = Instantiate(crowdPeepPrefab, spawnPosition, Quaternion.Euler(0, 0, 0));
            newPeepMain.GetComponent<CrowdCustomizer>().DestinationX = xPosition;
            newPeepMain.GetComponent<CrowdCustomizer>().MoveOnPlaza();
            crowdPeepsMain.Add(newPeepMain);
            
            
            //Secondly create a Peep at the riverside:
            float xPositionRiver = Random.Range(riverMinX, riverMaxX);
            float yPositionRiver = Random.Range(riverMinY, riverMaxY);
            
            shouldRecalculate = true;
            whileLimiter = 0;
            
            while (shouldRecalculate && whileLimiter < 200)
            {
                xPositionRiver = Random.Range(riverMinX, riverMaxX);
                yPositionRiver = Random.Range(riverMinY, riverMaxY);
                
                shouldRecalculate = false;
                foreach (GameObject go in crowdPeepsRiver)
                {
                    float distance = Vector3.Distance(new Vector3(xPositionRiver, yPositionRiver),
                        new Vector3(go.GetComponent<CrowdCustomizer>().DestinationX, go.transform.position.y));
                    if (distance < minDistanceBetweenPeeps)
                    {
                        shouldRecalculate = true;
                    }
                }

                whileLimiter += 1;
            }
            
            Vector3 spawnPositionRiver = new Vector3(xPositionRiver - newPeepSpawnDistance, yPositionRiver);
            GameObject newPeepRiver = Instantiate(crowdPeepPrefab, spawnPositionRiver, Quaternion.Euler(0, 0, 0));
            newPeepRiver.GetComponent<CrowdCustomizer>().DestinationX = xPositionRiver;
            newPeepRiver.GetComponent<CrowdCustomizer>().MoveOnPlaza();
            crowdPeepsRiver.Add(newPeepRiver);
        }

        private void destroyCrowdPeep()
        {
            if (crowdPeepsRiver.Count > 0)
            {
                int chosenPeep = Random.Range(0, crowdPeepsRiver.Count);
                CrowdCustomizer riverPeep = crowdPeepsRiver[chosenPeep].GetComponent<CrowdCustomizer>();
                CrowdCustomizer mainPeep = crowdPeepsMain[chosenPeep].GetComponent<CrowdCustomizer>();

                riverPeep.MoveOffPlaza();
                mainPeep.MoveOffPlaza();

                crowdPeepsRiver.Remove(riverPeep.gameObject);
                crowdPeepsMain.Remove(mainPeep.gameObject);
            }
        }

        private void OnPerfectScores()
        {
            
        }
    }
}