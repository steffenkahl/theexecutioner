﻿using System;
using System.Collections.Generic;
using PTWO_PR.Events;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class WeaponShopManager : MonoBehaviour
    {
        [SerializeField] private List<Weapon> allWeapons;
        [SerializeField] private int selectedWeapon;
        [SerializeField] private Image uiImage;
        [SerializeField] private LocalizeString weaponName;
        
        [Header("Store Elements")]
        [SerializeField] private Button buyButton;
        [SerializeField] private LocalizeString buyButtonText;
        [SerializeField] private GameObject buyTag;
        [SerializeField] private TMP_Text buyTagText;
        [SerializeField] private LocalizedString buyButtonLocalizedString;
        [SerializeField] private LocalizedString alreadyBoughtLocalizedString;
        [SerializeField] private LocalizedString tooExpensiveLocalizedString;
        
        public static WeaponShopManager Instance;
        
        public List<Weapon> AllWeapons => allWeapons;

        private void Awake()
        {
            Instance = this;
            selectedWeapon = 0;
        }
        
        private void OnValidate()
        {
            PopulateList();
        }
        
        private void Start()
        {
            UpdateSelectedWeapon();
        }

        private void OnEnable()
        {
            Message<SaveLoadedEvent>.Add(OnSaveLoadedEvent);
            Message<MoneyChangedEvent>.Add(OnMoneyChangedEvent);
        }
        
        private void OnDisable()
        {
            Message<SaveLoadedEvent>.Remove(OnSaveLoadedEvent);
            Message<MoneyChangedEvent>.Remove(OnMoneyChangedEvent);
        }

        public void OnNextWeapon()
        {
            if (selectedWeapon >= allWeapons.Count - 1)
            {
                selectedWeapon = 0;
            }
            else
            {
                selectedWeapon += 1;
            }
            
            UpdateSelectedWeapon();
        }
        
        public void OnPreviousWeapon()
        {
            if (selectedWeapon == 0)
            {
                selectedWeapon = allWeapons.Count - 1;
            }
            else
            {
                selectedWeapon -= 1;
            }
            
            UpdateSelectedWeapon();
        }

        private void OnSaveLoadedEvent(SaveLoadedEvent ctx)
        {
            PopulateList();
            UpdateSelectedWeapon();
        }

        private void OnMoneyChangedEvent(MoneyChangedEvent ctx)
        {
            UpdateSelectedWeapon();
        }
        
        private void Reset()
        {
            allWeapons = new List<Weapon>();
            PopulateList();
        }

        private void PopulateList()
        {
            #if UNITY_EDITOR
            string[] assetNames = AssetDatabase.FindAssets("t:ScriptableObject",
                new[] {"Assets/_GAMEASSETS/06-Objects/Weapons"});
            allWeapons.Clear();
            foreach (string sOName in assetNames)
            {
                string sOpath = AssetDatabase.GUIDToAssetPath(sOName);
                Weapon weapon = AssetDatabase.LoadAssetAtPath<Weapon>(sOpath);
                allWeapons.Add(weapon);
            }
            #endif
        }

        private void UpdateSelectedWeapon()
        {
            Weapon selectedWeaponObject = allWeapons[selectedWeapon];
            uiImage.sprite = selectedWeaponObject.WeaponSprite;
            weaponName.ChangeText(selectedWeaponObject.WeaponNameKey);

            if (SaveManager.Instance.WeaponsAvailable[selectedWeapon] || selectedWeaponObject.BoughtAtStart)
            {
                buyTag.SetActive(false);
                buyButton.interactable = false;
                buyButtonText.ChangeText(alreadyBoughtLocalizedString);
            }
            else if(SaveManager.Instance.MoneyAmount >= selectedWeaponObject.WeaponCost)
            {
                buyTag.SetActive(true);
                buyTagText.text = selectedWeaponObject.WeaponCost + "<sprite name=\"coin\" tint>"; //Display a sprite behind the text
                buyButton.interactable = true;
                buyButtonText.ChangeText(buyButtonLocalizedString);
            }
            else
            {
                buyTag.SetActive(true);
                buyTagText.text = selectedWeaponObject.WeaponCost + "<sprite name=\"coin\" tint>"; //Display a sprite behind the text
                buyButton.interactable = false;
                buyButtonText.ChangeText(tooExpensiveLocalizedString);
            }
        }

        public void OnBuyWeapon()
        {
            Weapon selectedWeaponObject = allWeapons[selectedWeapon];
            if (SaveManager.Instance.WeaponsAvailable[selectedWeapon] || selectedWeaponObject.BoughtAtStart || SaveManager.Instance.MoneyAmount < selectedWeaponObject.WeaponCost)
            {
                buyTag.SetActive(false);
                buyButton.interactable = false;
                buyButtonText.ChangeText(alreadyBoughtLocalizedString);
            }
            else
            {
                Message.Raise(new MoneyChangeEvent(-selectedWeaponObject.WeaponCost));
                SaveManager.Instance.WeaponsAvailable[selectedWeapon] = true;
                Message.Raise(new SaveEvent());
                Message.Raise(new LoadEvent());
                WeaponManager.Instance.PopulateList();
                Message.Raise(new IncreaseStatisticEvent(Statistic.WEAPONSBOUGHT_TOTAL, 1));
                Message.Raise(new IncreaseStatisticEvent(Statistic.WEAPONSBOUGHT_LIFE, 1));
                Message.Raise(new IncreaseStatisticEvent(Statistic.MONEYSPEND, selectedWeaponObject.WeaponCost));
            }
            
            UpdateSelectedWeapon();
        }
    }
}