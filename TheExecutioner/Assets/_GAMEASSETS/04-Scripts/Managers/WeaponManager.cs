﻿using System;
using System.Collections.Generic;
using PTWO_PR.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class WeaponManager : MonoBehaviour
    {
        [SerializeField] private List<Weapon> boughtWeapons;
        [SerializeField] private int selectedWeapon;
        [SerializeField] private Image uiImage;
        [SerializeField] private TMP_Text weaponName;
        [SerializeField] private Slider dMGSlider;
        [SerializeField] private Slider bRTSlider;
        [SerializeField] private Slider sHWSlider;

        public static WeaponManager Instance;
        private Weapon chosenWeapon;

        public Weapon ChosenWeapon
        {
            get => chosenWeapon;
            set => chosenWeapon = value;
        }

        private void OnEnable()
        {
            Message<SaveLoadedEvent>.Add(OnSaveLoadedEvent);
            Message<ClearSaveEvent>.Add(OnClearSaveEvent);
            Message<AfterFailureEvent>.Add(OnAfterFailureEvent);
            Message<CameraChangedEvent>.Add(OnCameraChangedEvent);
            Message<FailureEvent>.Add(OnFailureEvent);
        }

        private void OnDisable()
        {
            Message<SaveLoadedEvent>.Remove(OnSaveLoadedEvent);
            Message<ClearSaveEvent>.Remove(OnClearSaveEvent);
            Message<AfterFailureEvent>.Remove(OnAfterFailureEvent);
            Message<CameraChangedEvent>.Remove(OnCameraChangedEvent);
            Message<FailureEvent>.Remove(OnFailureEvent);
        }

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            PopulateList();
            selectedWeapon = 0;
            UpdateSelectedWeapon();
        }

        public void OnNextWeapon()
        {
            if (selectedWeapon >= boughtWeapons.Count - 1)
            {
                selectedWeapon = 0;
            }
            else
            {
                selectedWeapon += 1;
            }
            
            UpdateSelectedWeapon();
        }
        
        public void OnPreviousWeapon()
        {
            if (selectedWeapon == 0)
            {
                selectedWeapon = boughtWeapons.Count - 1;
            }
            else
            {
                selectedWeapon -= 1;
            }
            
            UpdateSelectedWeapon();
        }

        public void OnSelectWeapon()
        {
            chosenWeapon = boughtWeapons[selectedWeapon];
            Message.Raise(new SelectedWeaponEvent(chosenWeapon));
            Message.Raise(new ChangeCameraEvent(chosenWeapon.UsedScene));
        }

        private void OnSaveLoadedEvent(SaveLoadedEvent ctx)
        {
            PopulateList();
        }

        private void OnClearSaveEvent(ClearSaveEvent ctx)
        {
            Reset();
            selectedWeapon = 0;
            chosenWeapon = boughtWeapons[selectedWeapon];
            Message.Raise(new SelectedWeaponEvent(chosenWeapon));
            UpdateSelectedWeapon();
        }

        private void OnFailureEvent(FailureEvent ctx)
        {
            Reset();
            chosenWeapon = boughtWeapons[0];
        }

        private void OnAfterFailureEvent(AfterFailureEvent ctx)
        {
            Reset();
            selectedWeapon = 0;
            chosenWeapon = boughtWeapons[selectedWeapon];
            Message.Raise(new SelectedWeaponEvent(chosenWeapon));
        }
        
        private void Reset()
        {
            boughtWeapons = new List<Weapon>();
            PopulateList();
        }

        private void OnCameraChangedEvent(CameraChangedEvent ctx)
        {
            if (ctx.NewCamera == Cameras.WEAPONS)
            {
                boughtWeapons = new List<Weapon>();
                PopulateList();
                if (selectedWeapon > boughtWeapons.Count)
                {
                    selectedWeapon = 0;
                }
                UpdateSelectedWeapon();
            }
        }

        public void PopulateList()
        {
            List<Weapon> possibleWeapons = WeaponShopManager.Instance.AllWeapons;
            
            boughtWeapons.Clear();
            for (int i = 0; i < possibleWeapons.Count; i++)
            {
                if (possibleWeapons[i].BoughtAtStart)
                {
                    boughtWeapons.Add(possibleWeapons[i]);
                }
                else if (SaveManager.Instance.WeaponsAvailable[i])
                {
                    boughtWeapons.Add(possibleWeapons[i]);
                }
            }
        }

        private void UpdateSelectedWeapon()
        {
            Weapon selectedWeaponObject = boughtWeapons[selectedWeapon];
            uiImage.sprite = selectedWeaponObject.WeaponSprite;
            weaponName.GetComponent<LocalizeString>().ChangeText(selectedWeaponObject.WeaponNameKey);

            dMGSlider.value = selectedWeaponObject.Dmg;
            bRTSlider.value = selectedWeaponObject.Brt;
            sHWSlider.value = selectedWeaponObject.Shw;
        }
    }
}