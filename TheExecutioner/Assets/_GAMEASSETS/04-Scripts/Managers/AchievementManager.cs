using System;
using System.Collections.Generic;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class AchievementManager : MonoBehaviour
    {
        [SerializeField] private Achievement[] achievements;
        [SerializeField] private GameObject achListItemPrefab;
        [SerializeField] private Transform achHolder;
        private Dictionary<Achievement, bool> achievementEarned;
        private static AchievementManager instance;

        public static AchievementManager Instance => instance;
        public Achievement[] Achievements => achievements;

        private void OnEnable()
        {
            Message<IncreaseStatisticFinishedEvent>.Add(OnIncreaseStatisticFinishedEvent);
            Message<CameraChangedEvent>.Add(OnCameraChangedEvent);
            Message<ClearSaveEvent>.Add(OnClearSavesEvent);
        }

        private void OnDisable()
        {
            Message<IncreaseStatisticFinishedEvent>.Remove(OnIncreaseStatisticFinishedEvent);
            Message<CameraChangedEvent>.Remove(OnCameraChangedEvent);
            Message<ClearSaveEvent>.Remove(OnClearSavesEvent);
        }

        private void Awake()
        {
            if (GameObject.FindGameObjectWithTag(GameTags.ACHIEVEMENTMANAGER) != this.gameObject)
            {
                Destroy(this);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(this);
            }
            
            achievementEarned = new Dictionary<Achievement, bool>();
            foreach (Achievement a in achievements)
            {
                //First add every Achievement to a Dictionary
                achievementEarned.Add(a, false);
                //And spawn a corresponding AchievementListItem for viewing
                GameObject newInstantiatedAch = Instantiate(achListItemPrefab, achHolder);
                newInstantiatedAch.GetComponent<AchievementCustomizer>().SetAchievement(a);
                CheckIfAchievementIsAccomplished(a, true);
            }
        }

        private void OnCameraChangedEvent(CameraChangedEvent ctx)
        {
            foreach (Achievement ach in achievements)
            {
                CheckIfAchievementIsAccomplished(ach);
            }
        }

        private void OnIncreaseStatisticFinishedEvent(IncreaseStatisticFinishedEvent ctx)
        {
            if (StatisticsManager.Instance.Statistics.ContainsKey(ctx.StatisticToIncrease))
            {
                foreach (Achievement ach in achievements)
                {
                    if (ach.RequiredStatistic == ctx.StatisticToIncrease && !hasAchievement(ach))
                    {
                        CheckIfAchievementIsAccomplished(ach);
                    }
                }
            }
        }

        private void CheckIfAchievementIsAccomplished(Achievement achToCheck, bool silent = false)
        {
            if (achToCheck.RequiredAmount <= StatisticsManager.Instance.GetStatistic(achToCheck.RequiredStatistic))
            {
                if (!hasAchievement(achToCheck))
                {
                    SetHasAchievement(achToCheck, true);
                    Message.Raise(new EarnAchievementEvent(achToCheck, silent));
                }
            }
        }

        public bool hasAchievement(Achievement achIn)
        {
            if (achievementEarned.ContainsKey(achIn))
            {
                achievementEarned.TryGetValue(achIn, out bool value);
                return value;
            }
            return false;
        }
        
        private void SetHasAchievement(Achievement achIn, bool value)
        {
            if (achievementEarned.ContainsKey(achIn))
            {
                achievementEarned[achIn] = value;
            }
        }

        public void OnBackToLastSceneButton()
        {
            Message.Raise(new BackToLastCameraEvent());
        }

        private void OnClearSavesEvent(ClearSaveEvent ctx)
        {
            achievementEarned.Clear();
            foreach (Achievement a in achievements)
            {
                //First add every Achievement to a Dictionary
                achievementEarned.Add(a, false);
                //And spawn a corresponding AchievementListItem for viewing
                GameObject newInstantiatedAch = Instantiate(achListItemPrefab, achHolder);
                newInstantiatedAch.GetComponent<AchievementCustomizer>().SetAchievement(a);
                CheckIfAchievementIsAccomplished(a, true);
            }
        }
    }
}