﻿using System;
using PTWO_PR.Events;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class MagicManager : MonoBehaviour
    {
        [Header("References")] 
        [SerializeField] private GameObject emptySelectable;
        [SerializeField] private GameObject stageFeverSelectable;
        [SerializeField] private GameObject magicTrowelSelectable;
        [SerializeField] private GameObject magicTrowelText;

        [Header("Prices")] 
        [SerializeField] private int magicTrowelCost;
        [SerializeField] private int stageFeverCost;

        [SerializeField] private GameObject buyTagMagicTrowel;
        [SerializeField] private GameObject buyTagStageFever;
        [SerializeField] private GameObject buyButtonMagicTrowel;
        [SerializeField] private GameObject buyButtonStageFever;
        [SerializeField] private GameObject magicTrowelBoughtHint;
        [SerializeField] private GameObject stageFeverBoughtHint;
        
        [Header("Bought")]
        [SerializeField] private bool hasBoughtMagicTrowel;
        [SerializeField] private bool hasBoughtStageFever;

        [SerializeField] private Toggle magicTrowelToggle;
        [SerializeField] private Toggle stageFeverToggle;
        private bool magicTrowelActive;
        private bool stageFeverActive;

        public bool StageFeverActive => stageFeverActive;
        public bool MagicTrowelActive => magicTrowelActive;

        private void OnEnable()
        {
            Message<SaveLoadedEvent>.Add(OnSaveLoadedEvent);
            Message<CameraChangedEvent>.Add(OnCameraChangedEvent);
            Message<ClearSaveEvent>.Add(OnClearSaveEvent);
        }
        private void OnDisable()
        {
            Message<SaveLoadedEvent>.Remove(OnSaveLoadedEvent);
            Message<CameraChangedEvent>.Remove(OnCameraChangedEvent);
            Message<ClearSaveEvent>.Remove(OnClearSaveEvent);
        }

        private void Start()
        {
            emptySelectable.SetActive(true);
            stageFeverSelectable.SetActive(false);
            magicTrowelSelectable.SetActive(false);
            CheckForBoughtItems();
        }

        private void Update()
        {
            if (hasBoughtMagicTrowel)
            {
                magicTrowelSelectable.SetActive(true);
                buyButtonMagicTrowel.SetActive(false);
                buyTagMagicTrowel.SetActive(false);
                magicTrowelBoughtHint.SetActive(true);
            }
            else
            {
                magicTrowelSelectable.SetActive(false);
                buyButtonMagicTrowel.SetActive(true);
                buyTagMagicTrowel.SetActive(true);
                magicTrowelBoughtHint.SetActive(false);
            }

            if (hasBoughtStageFever)
            {
                stageFeverSelectable.SetActive(true);
                buyButtonStageFever.SetActive(false);
                buyTagStageFever.SetActive(false);
                stageFeverBoughtHint.SetActive(true);
            }
            else
            {
                stageFeverSelectable.SetActive(false);
                buyButtonStageFever.SetActive(true);
                buyTagStageFever.SetActive(true);
                stageFeverBoughtHint.SetActive(false);
            }

            if (!hasBoughtMagicTrowel && !hasBoughtStageFever)
            {
                emptySelectable.SetActive(true);
            }
            else
            {
                emptySelectable.SetActive(false);
            }
        }

        private void OnClearSaveEvent(ClearSaveEvent ctx)
        {
            hasBoughtMagicTrowel = false;
            hasBoughtStageFever = false;
        }

        private void OnSaveLoadedEvent(SaveLoadedEvent ctx)
        {
            CheckForBoughtItems();
        }

        private void OnCameraChangedEvent(CameraChangedEvent ctx)
        {
            CheckForBoughtItems();
        }

        private void CheckForBoughtItems()
        {
            if (SaveManager.Instance.Statistics != null)
            {
                if (SaveManager.Instance.Statistics.ContainsKey(Statistic.STAGEFEVERBOUGHT))
                {
                    if (StatisticsManager.Instance.GetStatistic(Statistic.STAGEFEVERBOUGHT) >= 1)
                    {
                        hasBoughtStageFever = true;
                    }
                }

                if (SaveManager.Instance.Statistics.ContainsKey(Statistic.MAGICTROWELBOUGHT))
                {
                    if (StatisticsManager.Instance.GetStatistic(Statistic.MAGICTROWELBOUGHT) >= 1)
                    {
                        hasBoughtMagicTrowel = true;
                    }
                }
            }
        }

        public void BuyMagicTrowel()
        {
            if (SaveManager.Instance.MoneyAmount >= magicTrowelCost)
            {
                hasBoughtMagicTrowel = true;
                Message.Raise(new MoneyChangeEvent(-magicTrowelCost));
                magicTrowelText.SetActive(magicTrowelToggle);
                magicTrowelToggle.isOn = true;
                Message.Raise(new IncreaseStatisticEvent(Statistic.MAGICTROWELBOUGHT, 1));
                Message.Raise(new IncreaseStatisticEvent(Statistic.MONEYSPEND, magicTrowelCost));
            }
        }
        
        public void BuyStageFever()
        {
            if (SaveManager.Instance.MoneyAmount >= stageFeverCost)
            {
                hasBoughtStageFever = true;
                Message.Raise(new MoneyChangeEvent(-stageFeverCost));
                Message.Raise(new IncreaseStatisticEvent(Statistic.STAGEFEVERBOUGHT, 1));
                Message.Raise(new IncreaseStatisticEvent(Statistic.MONEYSPEND, stageFeverCost));
            }
        }

        public void ChangeMagicTrowelActive()
        {
            if (hasBoughtMagicTrowel)
            {
                magicTrowelActive = magicTrowelToggle.isOn;
                magicTrowelText.SetActive(magicTrowelActive);
                if (magicTrowelActive)
                {
                    Message.Raise(new IncreaseStatisticEvent(Statistic.MAGICTROWELACTIVATED, 1));
                }
            }
        }
        
        public void ChangeStageFeverActive()
        {
            if (hasBoughtStageFever)
            {
                stageFeverActive = stageFeverToggle.isOn;
                if (stageFeverActive)
                {
                    Message.Raise(new IncreaseStatisticEvent(Statistic.STAGEFEVERACTIVATED, 1));
                }
            }
        }
    }
}