using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class StatisticsManager : MonoBehaviour
    {
        [SerializeField] private List<Statistic> statisticsToSave;
        private Dictionary<Statistic, int> statistics;
        private static StatisticsManager instance;

        public static StatisticsManager Instance => instance;

        public Dictionary<Statistic, int> Statistics
        {
            get => statistics;
            set => statistics = value;
        }

        private void Awake()
        {
            if (GameObject.FindGameObjectWithTag(GameTags.STATISTICSMANAGER) != this.gameObject)
            {
                Destroy(this);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(this);
            }
            
            statistics = new Dictionary<Statistic, int>();
            foreach (Statistic stat in statisticsToSave)
            {
                statistics.Add(stat, 0);
            }
        }

        private void OnEnable()
        {
            Message<IncreaseStatisticEvent>.Add(OnIncreaseStatisticEvent);
            Message<MoneyChangeEvent>.Add(OnMoneyChangeEvent);
            Message<FinishedPardonEvent>.Add(OnFinishedPardonEvent);
            Message<AfterFailureEvent>.Add(OnAfterFailureEvent);
            Message<SaveLoadedEvent>.Add(OnSaveLoadedEvent);
            Message<ClearSaveEvent>.Add(OnClearSavesEvent);
        }
        private void OnDisable()
        {
            Message<IncreaseStatisticEvent>.Remove(OnIncreaseStatisticEvent);
            Message<MoneyChangeEvent>.Remove(OnMoneyChangeEvent);
            Message<FinishedPardonEvent>.Remove(OnFinishedPardonEvent);
            Message<AfterFailureEvent>.Remove(OnAfterFailureEvent);
            Message<SaveLoadedEvent>.Remove(OnSaveLoadedEvent);
            Message<ClearSaveEvent>.Remove(OnClearSavesEvent);
        }

        /// <summary>
        /// Gets called whenever the Money Amount changes
        /// </summary>
        /// <param name="ctx"></param>
        private void OnMoneyChangeEvent(MoneyChangeEvent ctx)
        {
            if (ctx.Amount > 0)
            {
                Message.Raise(new IncreaseStatisticEvent(Statistic.MONEYEARNED_LIFE, ctx.Amount));
                Message.Raise(new IncreaseStatisticEvent(Statistic.MONEYEARNED_TOTAL, ctx.Amount));
            }
        }

        private void OnSaveLoadedEvent(SaveLoadedEvent ctx)
        {
            if (ctx.ActiveSaveManager.Statistics != null)
            {
                statistics = ctx.ActiveSaveManager.Statistics;
                Debug.Log(statistics[Statistic.VICTIMSKILLED_TOTAL]);
            }
        }

        private void OnAfterFailureEvent(AfterFailureEvent ctx)
        {
            foreach (KeyValuePair<Statistic, int> stat in statistics)
            {
                //Resets all Statistics that end on "_LIFE"
                if (stat.Key.ToString().EndsWith("_LIFE"))
                {
                    statistics[stat.Key] = 0;
                }
            }
        }

        /// <summary>
        /// Gets called whenever the player pardons a victim
        /// </summary>
        /// <param name="ctx"></param>
        private void OnFinishedPardonEvent(FinishedPardonEvent ctx)
        {
            Message.Raise(new IncreaseStatisticEvent(Statistic.VICTIMSPARDONED_LIFE, 1));
            Message.Raise(new IncreaseStatisticEvent(Statistic.VICTIMSPARDONED_TOTAL, 1));
        }

        /// <summary>
        /// Gets called whenever a statistic should get increased
        /// </summary>
        /// <param name="ctx"></param>
        private void OnIncreaseStatisticEvent(IncreaseStatisticEvent ctx)
        {
            if (statistics.ContainsKey(ctx.StatisticToIncrease))
            {
                statistics[ctx.StatisticToIncrease] += ctx.Amount;
                Message.Raise(new IncreaseStatisticFinishedEvent(ctx.StatisticToIncrease, ctx.Amount, statistics));
            }
        }

        /// <summary>
        /// Returns the BetterDictionary-Version of a specific statistic
        /// </summary>
        /// <param name="statisticIn"></param>
        /// <returns>(BetterDictionary of type Statistic,int) saved s</returns>
        public int GetStatistic(Statistic statisticIn)
        {
            if (statistics.ContainsKey(statisticIn))
            {
                statistics.TryGetValue(statisticIn, out int value);
                return value;
            }

            return 0;
        }

        private void OnClearSavesEvent(ClearSaveEvent ctx)
        {
            ResetStatistics();
        }

        public void ResetStatistics()
        {
            statistics.Clear();
            foreach (Statistic stat in statisticsToSave)
            {
                statistics.Add(stat, 0);
            }
        }
    }
}