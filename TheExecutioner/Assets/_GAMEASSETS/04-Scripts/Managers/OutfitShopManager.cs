﻿using System;
using System.Collections.Generic;
using PTWO_PR.Events;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class OutfitShopManager : MonoBehaviour
    {
        [SerializeField] private List<Outfit> allClothes;
        [SerializeField] private int selectedCloth;
        [SerializeField] private Image uiImage;
        [SerializeField] private Image uiImageBack;
        [SerializeField] private Image uiImageFront;
        [SerializeField] private TMP_Text clothName;
        
        [Header("Store Elements")]
        [SerializeField] private Button buyButton;
        [SerializeField] private LocalizeString buyButtonText;
        [SerializeField] private GameObject buyTag;
        [SerializeField] private TMP_Text buyTagText;
        [SerializeField] private LocalizedString buyButtonLocalizedString;
        [SerializeField] private LocalizedString alreadyBoughtLocalizedString;
        [SerializeField] private LocalizedString tooExpensiveLocalizedString;
        
        public static OutfitShopManager Instance;
        
        public List<Outfit> AllClothes => allClothes;
        
        private void Awake()
        {
            Instance = this;
            selectedCloth = 0;
        }
        
        //Use OnValidate to fill the list of possible clothes in the Editor automatically
        private void OnValidate()
        {
            PopulateList();
        }

        //Update the selected Cloth right at the beginning
        private void Start()
        {
            UpdateSelectedCloth();
        }

        //Subscribe to necessary events on OnEnable
        private void OnEnable()
        {
            Message<SaveLoadedEvent>.Add(OnSaveLoadedEvent);
            Message<MoneyChangedEvent>.Add(OnMoneyChangedEvent);
        }
        
        //Unsubscribe if the GO is ever gonna be Disabled
        private void OnDisable()
        {
            Message<SaveLoadedEvent>.Remove(OnSaveLoadedEvent);
            Message<MoneyChangedEvent>.Remove(OnMoneyChangedEvent);
        }

        /// <summary>
        /// Change the selected Cloth to the next one
        /// </summary>
        public void OnNextCloth()
        {
            if (selectedCloth >= allClothes.Count - 1)
            {
                selectedCloth = 0;
            }
            else
            {
                selectedCloth += 1;
            }
            
            UpdateSelectedCloth();
        }
        
        /// <summary>
        /// Change the selected Cloth to the previous one
        /// </summary>
        public void OnPreviousCloth()
        {
            if (selectedCloth == 0)
            {
                selectedCloth = allClothes.Count - 1;
            }
            else
            {
                selectedCloth -= 1;
            }
            
            UpdateSelectedCloth();
        }

        //If a new save is loaded, the state of the selected cloth has most certainly changed. So update it
        private void OnSaveLoadedEvent(SaveLoadedEvent ctx)
        {
            UpdateSelectedCloth();
        }

        //If the money got changed, the selected cloth could have changed too. So update it
        private void OnMoneyChangedEvent(MoneyChangedEvent ctx)
        {
            UpdateSelectedCloth();
        }

        /// <summary>
        /// Gets used only in Editor to directly populate the List of possible Outfits
        /// </summary>
        private void PopulateList()
        {
            #if UNITY_EDITOR
            string[] assetNames = AssetDatabase.FindAssets("t:ScriptableObject",
                new[] {"Assets/_GAMEASSETS/06-Objects/Outfits"});
            allClothes.Clear();
            foreach (string sOName in assetNames)
            {
                string sOpath = AssetDatabase.GUIDToAssetPath(sOName);
                Outfit outfit = AssetDatabase.LoadAssetAtPath<Outfit>(sOpath);
                allClothes.Add(outfit);
            }
            #endif
        }

        /// <summary>
        /// Gets executed each time when the selected cloth state could have changed
        /// </summary>
        private void UpdateSelectedCloth()
        {
            Outfit selectedClothObject = allClothes[selectedCloth];
            clothName.GetComponent<LocalizeString>().ChangeText(selectedClothObject.ClothNameKey);
            uiImage.sprite = selectedClothObject.ClothSprite;
            if (selectedClothObject.PantsBack)
            {
                uiImageBack.sprite = selectedClothObject.PantsBack;
                uiImageBack.color = Color.white;
            } else if (selectedClothObject.ShirtArmBack)
            {
                uiImageBack.sprite = selectedClothObject.ShirtArmBack;
                uiImageBack.color = Color.white;
            } else
            {
                uiImageBack.sprite = null;
                uiImageBack.color = new Color(0,0,0,0);
            }
            
            if (selectedClothObject.ShirtArmFront)
            {
                uiImageFront.sprite = selectedClothObject.ShirtArmFront;
                uiImageFront.color = Color.white;
            } else
            {
                uiImageFront.sprite = null;
                uiImageFront.color = new Color(0,0,0,0);
            }
            

            if (SaveManager.Instance.ClothesAvailable[selectedCloth] || selectedClothObject.BoughtAtStart)
            {
                buyTag.SetActive(false);
                buyButton.interactable = false;
                buyButtonText.ChangeText(alreadyBoughtLocalizedString);
            }
            else if(SaveManager.Instance.MoneyAmount >= selectedClothObject.ClothCost)
            {
                buyTag.SetActive(true);
                buyTagText.text = selectedClothObject.ClothCost + "<sprite name=\"coin\" tint>"; //Display a sprite behind the text
                buyButton.interactable = true;
                buyButtonText.ChangeText(buyButtonLocalizedString);
            }
            else
            {
                buyTag.SetActive(true);
                buyTagText.text = selectedClothObject.ClothCost + "<sprite name=\"coin\" tint>"; //Display a sprite behind the text
                buyButton.interactable = false;
                buyButtonText.ChangeText(tooExpensiveLocalizedString);
            }
        }

        /// <summary>
        /// Gets executed if the player presses the buy Button at the clothing store
        /// </summary>
        public void OnBuyCloth()
        {
            Outfit selectedClothObject = allClothes[selectedCloth];
            if (SaveManager.Instance.ClothesAvailable[selectedCloth] || selectedClothObject.BoughtAtStart || SaveManager.Instance.MoneyAmount < selectedClothObject.ClothCost)
            {
                buyTag.SetActive(false);
                buyButton.interactable = false;
                buyButtonText.ChangeText(alreadyBoughtLocalizedString);
            }
            else
            {
                Message.Raise(new MoneyChangeEvent(-selectedClothObject.ClothCost));
                SaveManager.Instance.ClothesAvailable[selectedCloth] = true;
                Message.Raise(new SaveEvent());
                Message.Raise(new LoadEvent());
                OutfitManager.Instance.PopulateList();
                Message.Raise(new IncreaseStatisticEvent(Statistic.CLOTHESBOUGHT, 1));
                Message.Raise(new IncreaseStatisticEvent(Statistic.MONEYSPEND, selectedClothObject.ClothCost));
            }
            
            UpdateSelectedCloth();
        }
    }
}