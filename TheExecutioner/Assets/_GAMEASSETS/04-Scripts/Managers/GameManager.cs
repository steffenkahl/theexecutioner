using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using PTWO_PR.Events;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance; //making this a singelton
        
        [SerializeField]
        private Button executeButtonMain; //referencing the Execute-Button, which should only spawn when the weapon has been choosen by the player
        [SerializeField]
        private Button executeButtonRiver; //referencing the Pardon-Button, which should only spawn when the weapon has been choosen by the player
        
        [SerializeField]
        private Button pardonButtonMain; //referencing the Execute-Button, which should only spawn when the weapon has been choosen by the player
        [SerializeField]
        private Button pardonButtonRiver; //referencing the Pardon-Button, which should only spawn when the weapon has been choosen by the player

        [Range(0f,1f)][SerializeField] private float globalAnimationSpeed;

        [SerializeField] private int moneyForPardon;

        public float GlobalAnimationSpeed => globalAnimationSpeed;

        //Variables needed to compare the stats of the chosen Weapon and the current victim
        private float dmgDifference;
        private float brtDifference;
        private float showDifference;
        private float fDifference;
        private int iDifference;

        [SerializeField] private Image blackPanel;

        [SerializeField] private int maxMoney;

        private void Awake()
        {
            instance = this; //You can use GameManger.instance from every Script and will be able to have access to this GameManager
        }

        private void OnEnable()
        {
            Message<SelectedWeaponEvent>.Add(OnSelectedWeaponEvent);
        }
        private void OnDisable()
        {
            Message<SelectedWeaponEvent>.Remove(OnSelectedWeaponEvent);
        }

        // Start is called before the first frame update
        void Start()
        {
            executeButtonMain.gameObject.SetActive(false); //deactivating the button which should only spawn when the weapon has been choosen by the player
            executeButtonRiver.gameObject.SetActive(false);
            pardonButtonMain.gameObject.SetActive(false);
            pardonButtonRiver.gameObject.SetActive(false);
            blackPanel.DOFade(0f, 2f);
        }

        private void OnSelectedWeaponEvent(SelectedWeaponEvent ctx)
        {
            ActivateButton();
        }

        public void ActivateButton() //Activating the Button, this method is called in WeaponManager when the weapon has been selected
        {
            if (WeaponManager.Instance.ChosenWeapon.UsedScene == Cameras.RIVER)
            {
                executeButtonRiver.gameObject.SetActive(true);
                if (!FailureManager.Instance.IsInFailureMode)
                {
                    pardonButtonRiver.gameObject.SetActive(true);
                }
            }
            else
            {
                executeButtonMain.gameObject.SetActive(true); 
                if (!FailureManager.Instance.IsInFailureMode)
                {
                    pardonButtonMain.gameObject.SetActive(true);
                }
            }
        }

        public void Execution()
        {
            Message.Raise(new StartedExecutionEvent());
        }
        
        public void Pardoned()
        {
            Message.Raise(new StartedPardonEvent());
        }

        public void PardonedMoney()
        {
            Dialogue victim = VictimManager.Instance.ChosenDialogue;
            if (victim.CanBePardoned)
            {
                Message.Raise(new MoneyChangeEvent(moneyForPardon));
                Message.Raise(new FinishedPardonEvent(moneyForPardon));
            }
            Message.Raise(new FinishedPardonEvent(0));
        }

        public void CompareStats()
        {
            //References for the stats of the current victim
            Dialogue victim = VictimManager.Instance.ChosenDialogue;
            float victimDamage = victim.Dmg;
            float victimBrt = victim.Brt;
            float victimShow = victim.Shw;

            //References for the stats of the current weapon
            Weapon weapon = WeaponManager.Instance.ChosenWeapon;
            float weaponDamage = weapon.Dmg;
            float weaponBrt = weapon.Brt;
            float weaponShow = weapon.Shw;
          
            //Comparing the Damage Stat, to make it positive without using am multiplicator we use an if-clause asking which variable is bigger
            if (victimDamage > weaponDamage)
            { dmgDifference = victimDamage - weaponDamage; }
            else
            { dmgDifference = weaponDamage - victimDamage; }

            //Comparing the Brutatlity Stat, to make it positive without using am multiplicator we use an if-clause asking which variable is bigger
            if (victimBrt > weaponBrt)
            { brtDifference = victimBrt - weaponBrt; }
            else
            {  brtDifference = weaponBrt - victimBrt; }

            //Comparing the Show Stat, to make it positive without using am multiplicator we use an if-clause asking which variable is bigger
            if (victimShow > weaponShow)
            { showDifference = victimShow - weaponShow; }
            else
            { showDifference = weaponShow - victimShow; }

            //Adding the Differences
            fDifference = dmgDifference + brtDifference + showDifference;

            //Converting the Float Difference to an int. 27 being the biggest difference and 0 being the smallest
            iDifference = Convert.ToInt32(fDifference);

            int moneyGiven = (int) (maxMoney - 5.8 * iDifference + 0.388 * Mathf.Pow(iDifference,2) - 0.0087*Mathf.Pow(iDifference,3));
            Message.Raise(new MoneyChangeEvent(moneyGiven));
            Debug.Log("Money given: " + moneyGiven + " for difference: " + iDifference);
            
           executeButtonMain.gameObject.SetActive(false);
           executeButtonRiver.gameObject.SetActive(false);
           
           Message.Raise(new FinishedExecutionEvent(iDifference, moneyGiven));
        }

        public void Exit()
        {
            Message.Raise(new SaveEvent());
            Application.Quit();
            Debug.Log(message: " Game has been quit");
        }

        public void BackToMenu()
        {
            Message.Raise(new ChangeCameraEvent(Cameras.MENU));
        }
    }
}
