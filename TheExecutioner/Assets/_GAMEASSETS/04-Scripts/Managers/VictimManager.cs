using System;
using System.Collections.Generic;
using PTWO_PR.Events;
using TMPro;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PTWO_PR
{
    public class VictimManager : MonoBehaviour
    {
        private static VictimManager instance;
        
        [SerializeField] private TMP_Text victimName;
        [SerializeField] private LocalizeString victimDialogueText;
        [SerializeField] private LocalizeString magicTrowelText;
        [SerializeField] private List<Peeple> possiblePeeple;
        [SerializeField] private List<Dialogue> possibleDialogues;
        [SerializeField] private Peeple chosenPeep;
        [SerializeField] private Dialogue chosenDialogue;

        private Dialogue[] lastDialogue = new Dialogue[3];
        private Peeple[] lastPeep = new Peeple[3];

        public static VictimManager Instance => instance;

        public Dialogue ChosenDialogue => chosenDialogue;
        public Peeple ChosenPeep => chosenPeep;
        
        

        private void Awake()
        {
            instance = this;
            chosenDialogue = null;
            chosenPeep = null;
        }

        private void OnValidate()
        {
            PopulateList();
        }

        private void OnEnable()
        {
            Message<LoadNewVictimEvent>.Add(OnLoadNewVictimEvent);
        }
        
        private void OnDisable()
        {
            Message<LoadNewVictimEvent>.Remove(OnLoadNewVictimEvent);
        }

        private void Start()
        {
            LoadNewVictim();
        }

        private void PopulateList()
        {
            possibleDialogues = new List<Dialogue>();
            possiblePeeple = new List<Peeple>();
            
            #if UNITY_EDITOR
            string[] assetNamesPeeple = AssetDatabase.FindAssets("t:ScriptableObject",
                new[] {"Assets/_GAMEASSETS/06-Objects/Peeple"});
            foreach (string sOName in assetNamesPeeple)
            {
                string sOpath = AssetDatabase.GUIDToAssetPath(sOName);
                Peeple peep = AssetDatabase.LoadAssetAtPath<Peeple>(sOpath);
                possiblePeeple.Add(peep);
            }
            
            string[] assetNamesDialogues = AssetDatabase.FindAssets("t:ScriptableObject",
                new[] {"Assets/_GAMEASSETS/06-Objects/Dialogues"});
            foreach (string sOName in assetNamesDialogues)
            {
                string sOpath = AssetDatabase.GUIDToAssetPath(sOName);
                Dialogue dialogue = AssetDatabase.LoadAssetAtPath<Dialogue>(sOpath);
                possibleDialogues.Add(dialogue);
            }
            #endif
        }

        public void OnNextScene()
        {
            Message.Raise(new ChangeCameraEvent(Cameras.WEAPONS));
            Message.Raise(new VictimListenedEvent());
        }

        private void OnLoadNewVictimEvent(LoadNewVictimEvent ctx)
        {
            LoadNewVictim();
        }

        private void LoadNewVictim()
        {
            int whileCounter = 0;
            
            while (chosenDialogue == lastDialogue[0] || chosenDialogue == lastDialogue[1] || chosenDialogue == lastDialogue[2])
            {
                chosenDialogue = possibleDialogues[Random.Range(0, possibleDialogues.Count)];
                whileCounter += 1;
                if (whileCounter > 100)
                {
                    Debug.LogWarning("Exceeded While Loop Max");
                    break; //Prevent while loop from crashing game
                }
            }

            whileCounter = 0;
            while (chosenPeep == lastPeep[0] || chosenPeep == lastPeep[1] || chosenPeep == lastPeep[2])
            {
                chosenPeep = possiblePeeple[Random.Range(0, possiblePeeple.Count)];
                whileCounter += 1;
                if (whileCounter > 100)
                {
                    Debug.LogWarning("Exceeded While Loop Max");
                    break; //Prevent while loop from crashing game
                }
            }
            
            Message.Raise(new NewVictimLoadedEvent(chosenPeep, chosenDialogue));
            
            UpdateDisplay();

            //Save selected Dialogues and Peeple:
            lastDialogue[2] = lastDialogue[1];
            lastDialogue[1] = lastDialogue[0];
            lastDialogue[0] = chosenDialogue;

            lastPeep[2] = lastPeep[1];
            lastPeep[1] = lastPeep[0];
            lastPeep[0] = chosenPeep;
        }

        private void UpdateDisplay()
        {
            victimDialogueText.ChangeText(chosenDialogue.DialogueKey);
            magicTrowelText.ChangeText(chosenDialogue.MagicalTrowelTip);
            victimName.text = chosenPeep.PeepleName;
        }
    }
}