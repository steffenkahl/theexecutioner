using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using PTWO_PR.Events;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

namespace PTWO_PR
{
    public class SaveManager : MonoBehaviour
    {
        private static SaveManager instance;
        
        [SerializeField] private int moneyAmount;
        [SerializeField] private Locale selectedLanguage;
        [SerializeField] private float musicVolume;
        [SerializeField] private bool goreActivated;
        [SerializeField] private bool[] weaponsAvailable;
        [SerializeField] private bool[] clothesAvailable;
        [SerializeField] private int selectedHat;
        [SerializeField] private int selectedShirt;
        [SerializeField] private int selectedPants;
        [SerializeField] private int selectedDeco;
        [SerializeField] private int redFlags;
        [SerializeField] private bool tutorialFinished;
        private Dictionary<Statistic, int> statistics;

        public static SaveManager Instance => instance;

        public int MoneyAmount
        {
            get => moneyAmount;
            set => moneyAmount = value;
        }

        public int RedFlags => redFlags;

        public bool TutorialFinished
        {
            get => tutorialFinished;
            set => tutorialFinished = value;
        }

        //--Settings--
        public Locale SelectedLanguage => selectedLanguage;
        public float MusicVolume => musicVolume;

        public bool GoreActivated
        {
            get => goreActivated;
            set => goreActivated = value;
        }
        
        //--Clothes and Weapons--
        public bool[] WeaponsAvailable => weaponsAvailable;
        public bool[] ClothesAvailable => clothesAvailable;

        public int SelectedHat => selectedHat;
        public int SelectedShirt => selectedShirt;
        public int SelectedPants => selectedPants;
        public int SelectedDeco => selectedDeco;

        public Dictionary<Statistic, int> Statistics => statistics;

        private void Awake()
        {
            if (GameObject.FindGameObjectWithTag(GameTags.SAVEMANAGER) != this.gameObject)
            {
                Destroy(this);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(this);
            }

            weaponsAvailable = new bool[100];
            clothesAvailable = new bool[100];
        }

        private void Start()
        {
            LoadGame();
            StartCoroutine(DelayedLoad()); //Delay the first load to make sure the locale is already available
        }

        private void OnEnable()
        {
            Message<SaveEvent>.Add(OnSaveEvent);
            Message<LoadEvent>.Add(OnLoadEvent);
            Message<ClearSaveEvent>.Add(OnClearSaveEvent);
            Message<LanguageChangeEvent>.Add(OnLanguageChangeEvent);
            Message<FailureEvent>.Add(OnFailureEvent);
            Message<IncreaseStatisticFinishedEvent>.Add(OnIncreaseStatisticFinishedEvent);
            Message<KingBadImpressionEvent>.Add(OnKingBadImpressionEvent);
        }
        
        private void OnDisable()
        {
            Message<SaveEvent>.Remove(OnSaveEvent);
            Message<LoadEvent>.Remove(OnLoadEvent);
            Message<ClearSaveEvent>.Remove(OnClearSaveEvent);
            Message<LanguageChangeEvent>.Remove(OnLanguageChangeEvent);
            Message<FailureEvent>.Remove(OnFailureEvent);
            Message<IncreaseStatisticFinishedEvent>.Remove(OnIncreaseStatisticFinishedEvent);
            Message<KingBadImpressionEvent>.Remove(OnKingBadImpressionEvent);
        }

        private void OnIncreaseStatisticFinishedEvent(IncreaseStatisticFinishedEvent ctx)
        {
            statistics = ctx.StatisticDict;
        }

        private void OnFailureEvent(FailureEvent ctx)
        {
            moneyAmount = 0;
            weaponsAvailable = new bool[100];
        }
        
        private void OnSaveEvent(SaveEvent ctx)
        {
            SaveGame();
        }
        
        private void OnLoadEvent(LoadEvent ctx)
        {
            LoadGame();
        }

        private void OnClearSaveEvent(ClearSaveEvent ctx)
        {
            float oldVolume = musicVolume;
            bool oldGore = goreActivated;
            PlayerPrefs.DeleteAll();
            selectedDeco = 0;
            selectedHat = 0;
            selectedPants = 0;
            selectedShirt = 0;
            LoadGame();
            WeaponManager.Instance.PopulateList();
            ChangeMusicVolume(musicVolume);
            goreActivated = oldGore;
        }

        private void OnLanguageChangeEvent(LanguageChangeEvent ctx)
        {
            selectedLanguage = ctx.NewLocale;
        }

        private void OnKingBadImpressionEvent(KingBadImpressionEvent ctx)
        {
            redFlags = ctx.BadImpressions;
        }

        public void SaveGame()
        {
            PlayerPrefs.SetInt("moneyAmount", moneyAmount);
            if (selectedLanguage != null)
            {
                PlayerPrefs.SetString("selectedLanguage", selectedLanguage.Identifier.Code);
            }

            PlayerPrefs.SetFloat("musicVolume", musicVolume);
            PlayerPrefs.SetInt("goreActivated", goreActivated ? 1 : 0); //If true set 1. Else set 0
            PlayerPrefs.SetInt("tutorialFinished", tutorialFinished ? 1 : 0); //If true set 1. Else set 0
            for (int i = 0; i < weaponsAvailable.Length; i++)
            {
                PlayerPrefs.SetInt("availableWeapons-" + i, weaponsAvailable[i] ? 1 : 0);
            }
            for (int i = 0; i < clothesAvailable.Length; i++)
            {
                PlayerPrefs.SetInt("availableClothes-" + i, clothesAvailable[i] ? 1 : 0);
            }
            PlayerPrefs.SetInt("selectedHat", selectedHat);
            PlayerPrefs.SetInt("selectedShirt", selectedShirt);
            PlayerPrefs.SetInt("selectedPants", selectedPants);
            PlayerPrefs.SetInt("selectedDeco", selectedDeco);
            PlayerPrefs.SetInt("redFlags", redFlags);
            
            if (statistics != null)
            {
                //Saving Statistics is kinda tricky, because its a dictionary:
                //Slightly modified concept from https://www.dotnetperls.com/convert-dictionary-string
                string statisticsAsString = GetDictionaryAsString(statistics);
                PlayerPrefs.SetString("statistics", statisticsAsString);
            }
        }

        /// <summary>
        /// Gets a dictionary as a string
        /// </summary>
        /// <param name="data">Dictionary to translate</param>
        /// <typeparam name="TKey">Keytype of Dictionary</typeparam>
        /// <typeparam name="TValue">Valuetype of Dictionary</typeparam>
        /// <returns></returns>
        private string GetDictionaryAsString<TKey, TValue>(Dictionary<TKey, TValue> data)
        {
            // Create a StringBuilder to build a string out of the dictionary
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<TKey,TValue> pair in data)
            {
                builder.Append(pair.Key).Append( ":").Append(pair.Value).Append(',');
            }
            string result = builder.ToString();
            // Remove the end comma.
            result = result.TrimEnd(',');
            return result;
        }

        /// <summary>
        /// Gets a Dictionary<Enum, int> with a generic enum of any type
        /// </summary>
        /// <param name="file">string to transform</param>
        /// <typeparam name="TKey">Type of Enum to get</typeparam>
        /// <returns></returns>
        private Dictionary<TKey, int> GetEnumDictToIntFromString<TKey>(string file) where TKey : Enum
        {
            var result = new Dictionary<TKey, int>();
            string inputString = file;
            // Split the string.
            string[] tokens = inputString.Split(new char[] { ':', ',' }, StringSplitOptions.RemoveEmptyEntries);
            // Build up our dictionary from the string.
            for (int i = 0; i < tokens.Length; i += 2)
            {
                TKey key = (TKey)Enum.Parse(typeof(TKey), tokens[i]);
                int value = int.Parse(tokens[i + 1]);
            
                // Add the value to our dictionary.
                if (result.ContainsKey(key))
                {
                    result[key] = value;
                }
                else
                {
                    result.Add(key, value);
                }
            }
            return result;
        }
        
        IEnumerator DelayedLoad()
        {
            //Wait for the specified delay time before continuing.
            yield return new WaitForSeconds(0.5f);
 
            string languageCode = PlayerPrefs.GetString("selectedLanguage", "en");
            selectedLanguage = LocalizationSettings.AvailableLocales.GetLocale(languageCode);
            LoadGame();
        }
        
        private void LoadGame()
        {
            moneyAmount = PlayerPrefs.GetInt("moneyAmount", 0);
            
            LocalizationSettings.SelectedLocale = selectedLanguage;
            musicVolume = PlayerPrefs.GetFloat("musicVolume", 0.5f);
            goreActivated = PlayerPrefs.GetInt("goreActivated", 0) == 1; //1 stands for true and 0 for false
            tutorialFinished = PlayerPrefs.GetInt("tutorialFinished", 0) == 1; //1 stands for true and 0 for false
            for (int i = 0; i < weaponsAvailable.Length; i++)
            {
                weaponsAvailable[i] = PlayerPrefs.GetInt("availableWeapons-" + i, 0) == 1;
            }
            for (int i = 0; i < clothesAvailable.Length; i++)
            {
                clothesAvailable[i] = PlayerPrefs.GetInt("availableClothes-" + i, 0) == 1;
            }

            if (PlayerPrefs.HasKey("statistics"))
            {
                statistics = GetEnumDictToIntFromString<Statistic>(PlayerPrefs.GetString("statistics"));
            }

            selectedHat = PlayerPrefs.GetInt("selectedHat", 0);
            selectedShirt = PlayerPrefs.GetInt("selectedShirt", 0);
            selectedPants = PlayerPrefs.GetInt("selectedPants", 0);
            selectedDeco = PlayerPrefs.GetInt("selectedDeco", 0);
            
            Message.Raise(new SaveLoadedEvent(this));
        }

        public void ChangeSelectedClothes(ClothPlaces place, int selectedCloth)
        {
            //Gets called from OutfitManager after chosing clothes
            switch (place)
            {
                default:
                    break;
                case ClothPlaces.HAT:
                    selectedHat = selectedCloth;
                    break;
                case ClothPlaces.SHIRT:
                    selectedShirt = selectedCloth;
                    break;
                case ClothPlaces.PANTS:
                    selectedPants = selectedCloth;
                    break;
                case ClothPlaces.DECO:
                    selectedDeco = selectedCloth;
                    break;
            }
        }

        public void ChangeMusicVolume(float newVolume)
        {
            musicVolume = newVolume;
        }

        private void OnApplicationQuit()
        {
            SaveGame();
        }
    }
}