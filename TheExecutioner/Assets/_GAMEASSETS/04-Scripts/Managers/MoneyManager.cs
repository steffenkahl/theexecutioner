using System;
using System.Collections;
using System.Collections.Generic;
using PTWO_PR;
using PTWO_PR.Events;
using TMPro;
using UnityEngine;

public class MoneyManager : MonoBehaviour
{
    [SerializeField] private int currentMoney;
    [SerializeField] private TMP_Text currentMoneyText;

    private void OnEnable()
    {
        Message<MoneyChangeEvent>.Add(OnMoneyChangeEvent);
        Message<ClearSaveEvent>.Add(OnClearSaveEvent);
        Message<FailureEvent>.Add(OnFailureEvent);
    }

    private void OnDisable()
    {
        Message<MoneyChangeEvent>.Remove(OnMoneyChangeEvent);
        Message<ClearSaveEvent>.Remove(OnClearSaveEvent);
        Message<FailureEvent>.Remove(OnFailureEvent);
    }

    private void Start()
    {
        currentMoney = SaveManager.Instance.MoneyAmount;
        ChangeMoneyAmountDisplay();
    }

    private void OnMoneyChangeEvent(MoneyChangeEvent ctx)
    {
        currentMoney += ctx.Amount;
        
        if (currentMoney < 0)
        {
            //Prevent money from becoming negative
            currentMoney = 0;
        }
        
        ChangeMoneyAmountDisplay();
        SaveManager.Instance.MoneyAmount = currentMoney;
        Message.Raise(new SaveEvent());
        Message.Raise(new MoneyChangedEvent());
    }

    private void OnFailureEvent(FailureEvent ctx)
    {
        currentMoney = 0;
        ChangeMoneyAmountDisplay();
    }

    private void ChangeMoneyAmountDisplay()
    {
        currentMoneyText.text = currentMoney + "<sprite name=\"coin\" tint>"; //Display a sprite behind the text
    }

    private void OnClearSaveEvent(ClearSaveEvent ctx)
    {
        currentMoney = 0;
        ChangeMoneyAmountDisplay();
    }
}
