using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField] public CameraDictionary[] cameras;
        [SerializeField] private Cameras activeCamera;
        private Cameras lastCamera;
        
        public Cameras activeCamera1
        {
            get => activeCamera;
            set => activeCamera = value;
        }

        private void OnEnable()
        {
            Message<ChangeCameraEvent>.Add(OnCameraChange);
            Message<FinishedExecutionEvent>.Add(OnFinishedExecutionEvent);
            Message<FinishedPardonEvent>.Add(OnFinishedPardonEvent);
            Message<BackToLastCameraEvent>.Add(OnBackToLastCameraEvent);
        }

        private void OnDisable()
        {
            Message<ChangeCameraEvent>.Remove(OnCameraChange);
            Message<FinishedExecutionEvent>.Remove(OnFinishedExecutionEvent);
            Message<FinishedPardonEvent>.Remove(OnFinishedPardonEvent);
            Message<BackToLastCameraEvent>.Remove(OnBackToLastCameraEvent);
        }

        private void OnValidate()
        {
            UpdateCamera();
        }

        private void OnCameraChange(ChangeCameraEvent ctx)
        {
            ChangeCamera(ctx.NewCamera);
        }

        private void ChangeCamera(Cameras newCamera)
        {
            if (activeCamera != Cameras.NONE)
            {
                lastCamera = activeCamera;
            }
            activeCamera = newCamera;
            UpdateCamera();
        }

        private void OnBackToLastCameraEvent(BackToLastCameraEvent ctx)
        {
            ChangeCamera(lastCamera);
        }

        private void UpdateCamera()
        {
            foreach (CameraDictionary c in cameras)
            {
                if (c.GetKey() == activeCamera)
                {
                    c.GetValue().Priority = 10;
                }
                else
                {
                    c.GetValue().Priority = 0;
                }
            }
            Message.Raise(new CameraChangedEvent(activeCamera)); //Send a new message if change was successful
        }

        private void OnFinishedExecutionEvent(FinishedExecutionEvent ctx)
        {
            StartCoroutine(delayCamera());
        }
        
        IEnumerator delayCamera()
        {
            yield return new WaitForSeconds(2);
            Message.Raise(new ChangeCameraEvent(Cameras.DUNGEON));
        }
        
        private void OnFinishedPardonEvent(FinishedPardonEvent ctx)
        {
            Message.Raise(new ChangeCameraEvent(Cameras.DUNGEON));
        }

        public void OpenURL(string urlToOpen)
        {
            Application.OpenURL(urlToOpen);
        }
    }
}