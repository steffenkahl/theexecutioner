﻿using System;
using System.Collections.Generic;
using PTWO_PR.Events;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class OutfitManager : MonoBehaviour
    {
        [SerializeField] private List<Outfit> boughtHats;
        [SerializeField] private List<Outfit> boughtShirts;
        [SerializeField] private List<Outfit> boughtLegs;
        [SerializeField] private List<Outfit> boughtDecos;
        [SerializeField] private int selectedHat;
        [SerializeField] private int selectedShirt;
        [SerializeField] private int selectedLegs;
        [SerializeField] private int selectedDeco;
        [SerializeField] private Image uiImageHat;
        [SerializeField] private Image uiImageShirt;
        [SerializeField] private Image uiImageShirtBack;
        [SerializeField] private Image uiImageShirtFront;
        [SerializeField] private Image uiImagePantsFront;
        [SerializeField] private Image uiImagePantsBack;
        [SerializeField] private Image uiImageDeco;
        
        public static OutfitManager Instance;
        private Outfit chosenHat;
        private Outfit chosenShirt;
        private Outfit chosenLeg;
        private Outfit chosenDeco;

        public Outfit ChosenHat => chosenHat;
        public Outfit ChosenShirt => chosenShirt;
        public Outfit ChosenLeg => chosenLeg;
        public Outfit ChosenDeco => chosenDeco;

        private void OnEnable()
        {
            Message<SaveLoadedEvent>.Add(OnSaveLoadedEvent);
            Message<FailureActivatedEvent>.Add(OnFailureActivatedEvent);
        }
        
        private void OnDisable()
        {
            Message<SaveLoadedEvent>.Remove(OnSaveLoadedEvent);
            Message<FailureActivatedEvent>.Remove(OnFailureActivatedEvent);
        }

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            PopulateList();
            UpdateSelectedOutfit();
        }

        private void OnFailureActivatedEvent(FailureActivatedEvent ctx)
        {
            selectedHat = 0;
            selectedDeco = 0;
            selectedLegs = 0;
            selectedShirt = 0;
            OnSelectOutfit();
            UpdateSelectedOutfit();
        }

        public void OnNextHat()
        {
            if (selectedHat >= boughtHats.Count - 1)
            {
                selectedHat = 0;
            }
            else
            {
                selectedHat += 1;
            }
            
            UpdateSelectedOutfit();
        }
        
        public void OnPreviousHat()
        {
            if (selectedHat == 0)
            {
                selectedHat = boughtHats.Count - 1;
            }
            else
            {
                selectedHat -= 1;
            }
            
            UpdateSelectedOutfit();
        }
        
        public void OnNextShirt()
        {
            Debug.Log("Next Outfit");
            if (selectedShirt >= boughtShirts.Count - 1)
            {
                selectedShirt = 0;
            }
            else
            {
                selectedShirt += 1;
            }
            
            UpdateSelectedOutfit();
        }
        
        public void OnPreviousShirt()
        {
            if (selectedShirt == 0)
            {
                selectedShirt = boughtShirts.Count - 1;
            }
            else
            {
                selectedShirt -= 1;
            }
            
            UpdateSelectedOutfit();
        }
        
        public void OnNextPants()
        {
            if (selectedLegs >= boughtLegs.Count - 1)
            {
                selectedLegs = 0;
            }
            else
            {
                selectedLegs += 1;
            }
            
            UpdateSelectedOutfit();
        }
        
        public void OnPreviousPants()
        {
            if (selectedLegs == 0)
            {
                selectedLegs = boughtLegs.Count - 1;
            }
            else
            {
                selectedLegs -= 1;
            }
            
            UpdateSelectedOutfit();
        }
        
        public void OnNextDeco()
        {
            if (selectedDeco >= boughtDecos.Count - 1)
            {
                selectedDeco = 0;
            }
            else
            {
                selectedDeco += 1;
            }
            
            UpdateSelectedOutfit();
        }
        
        public void OnPreviousDeco()
        {
            if (selectedDeco == 0)
            {
                selectedDeco = boughtDecos.Count - 1;
            }
            else
            {
                selectedDeco -= 1;
            }
            
            UpdateSelectedOutfit();
        }

        public void OnSelectOutfit()
        {
            chosenHat = boughtHats[selectedHat];
            chosenShirt = boughtShirts[selectedShirt];
            chosenLeg = boughtLegs[selectedLegs];
            chosenDeco = boughtDecos[selectedDeco];
            Message.Raise(new OutfitChangedEvent());

            SaveManager.Instance.ChangeSelectedClothes(ClothPlaces.HAT, selectedHat);
            SaveManager.Instance.ChangeSelectedClothes(ClothPlaces.SHIRT, selectedShirt);
            SaveManager.Instance.ChangeSelectedClothes(ClothPlaces.PANTS, selectedLegs);
            SaveManager.Instance.ChangeSelectedClothes(ClothPlaces.DECO, selectedDeco);
            Message.Raise(new SaveEvent());
        }

        private void OnSaveLoadedEvent(SaveLoadedEvent ctx)
        {
            PopulateList();
            selectedHat = SaveManager.Instance.SelectedHat;
            selectedShirt = SaveManager.Instance.SelectedShirt;
            selectedLegs = SaveManager.Instance.SelectedPants;
            selectedDeco = SaveManager.Instance.SelectedDeco;
            chosenHat = boughtHats[selectedHat];
            chosenShirt = boughtShirts[selectedShirt];
            chosenLeg = boughtLegs[selectedLegs];
            chosenDeco = boughtDecos[selectedDeco];
            Message.Raise(new OutfitChangedEvent());
            OnSelectOutfit();
            UpdateSelectedOutfit();
        }
        
        private void Reset()
        {
            boughtHats = new List<Outfit>();
            boughtShirts = new List<Outfit>();
            boughtLegs = new List<Outfit>();
            PopulateList();
        }

        public void PopulateList()
        {
            List<Outfit> possibleClothes = OutfitShopManager.Instance.AllClothes;
            
            boughtHats.Clear();
            boughtShirts.Clear();
            boughtLegs.Clear();
            boughtDecos.Clear();
            
            for (int i = 0; i < possibleClothes.Count;  i++)
            {
                if (possibleClothes[i].BoughtAtStart || SaveManager.Instance.ClothesAvailable[i])
                {
                    switch (possibleClothes[i].ClothPlace)
                    {
                        default: 
                            boughtDecos.Add(possibleClothes[i]);
                            break;
                        case ClothPlaces.SHIRT:
                            boughtShirts.Add(possibleClothes[i]);
                            break;
                        case ClothPlaces.HAT:
                            boughtHats.Add(possibleClothes[i]);
                            break;
                        case ClothPlaces.PANTS:
                            boughtLegs.Add(possibleClothes[i]);
                            break;
                    }
                }
            }
            
            chosenHat = boughtHats[selectedHat];
            chosenShirt = boughtShirts[selectedShirt];
            chosenLeg = boughtLegs[selectedLegs];
            chosenDeco = boughtDecos[selectedDeco];
        }

        private void UpdateSelectedOutfit()
        {
            uiImageHat.sprite = boughtHats[selectedHat].ClothSprite;
            uiImageShirt.sprite = boughtShirts[selectedShirt].ClothSprite;
            uiImageShirtFront.sprite = boughtShirts[selectedShirt].ShirtArmFront;
            uiImageShirtBack.sprite = boughtShirts[selectedShirt].ShirtArmBack;
            uiImagePantsFront.sprite = boughtLegs[selectedLegs].ClothSprite;
            uiImagePantsBack.sprite = boughtLegs[selectedLegs].PantsBack;
            uiImageDeco.sprite = boughtDecos[selectedDeco].ClothSprite;
        }
    }
}