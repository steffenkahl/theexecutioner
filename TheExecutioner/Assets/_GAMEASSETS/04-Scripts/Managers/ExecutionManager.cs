using System;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class ExecutionManager : MonoBehaviour
    {
        [SerializeField] private Transform executionStageMain; //Reference for the Transform that holds the weapon Prefab on MainStage
        [SerializeField] private Transform executionStageRiver; //Reference for the Transform that holds the weapon Prefab at the River
        [SerializeField] private GameObject weaponEmptyPrefab; //Reference for the Prefab if no weapon is selected
        [SerializeField] private GameObject signOtherStagePrefab; //Reference to a Prefab that is shown when other stage is used

        private void OnEnable()
        {
            Message<SelectedWeaponEvent>.Add(OnSelectedWeaponEvent);
            Message<VictimListenedEvent>.Add(OnVictimListenedEvent);
        }
        
        private void OnDisable()
        {
            Message<SelectedWeaponEvent>.Remove(OnSelectedWeaponEvent);
            Message<VictimListenedEvent>.Remove(OnVictimListenedEvent);
        }

        private void Start()
        {
            ClearStage();
        }

        private void OnSelectedWeaponEvent(SelectedWeaponEvent ctx)
        {
            //First clear all prefabs:
            foreach (Transform child in executionStageMain) {
                Destroy(child.gameObject);
            }
            foreach (Transform child in executionStageRiver) {
                Destroy(child.gameObject);
            }

            ExecutionerAnimationHandler weaponPrefab;
            
            //Then spawn the needed Prefabs:
            if (ctx.SelectedWeapon.UsedScene == Cameras.RIVER)
            {
                weaponPrefab = Instantiate(ctx.SelectedWeapon.WeaponPrefab, executionStageRiver).GetComponent<ExecutionerAnimationHandler>(); //Spawn the selected Weapon Prefab
                Instantiate(signOtherStagePrefab, executionStageMain); //spawn sign on other stage
            }
            else
            {
                weaponPrefab = Instantiate(ctx.SelectedWeapon.WeaponPrefab, executionStageMain).GetComponent<ExecutionerAnimationHandler>(); //Spawn the selected Weapon Prefab
                Instantiate(signOtherStagePrefab, executionStageRiver); //spawn sign on other stage
            }
            
            weaponPrefab.UpdateVictim();
        }

        private void OnVictimListenedEvent(VictimListenedEvent ctx)
        {
            ClearStage();
        }

        private void ClearStage()
        {
            //First clear all prefabs
            foreach (Transform child in executionStageMain) {
                Destroy(child.gameObject);
            }
            foreach (Transform child in executionStageRiver) {
                Destroy(child.gameObject);
            }
            
            //Then spawn empty weapon prefab on both stages
            Instantiate(weaponEmptyPrefab, executionStageMain).GetComponent<ExecutionerAnimationHandler>(); 
            Instantiate(weaponEmptyPrefab, executionStageRiver).GetComponent<ExecutionerAnimationHandler>();
        }
    }
}