using System;
using System.Collections.Generic;
using DG.Tweening;
using PTWO_PR.Events;
using UnityEditor;
using UnityEngine;
using UnityEngine.Localization;

namespace PTWO_PR
{
    public class TutorialSystem : MonoBehaviour
    {
        [SerializeField] private LocalizeString tutorialTextfield;
        [SerializeField] private List<BetterDictionary<Cameras, List<LocalizedString>>> tutorialTexts;
        [SerializeField] private List<BetterDictionary<Cameras, bool>> tutorialPartCompleted;
        [SerializeField] private Animator ghostAnimator;
        [SerializeField] private bool tutorialCompleteFinished;
        [SerializeField] private List<BetterDictionary<Cameras, GameObject>> ghostPositions;
        [SerializeField] private GameObject ghostGO;
        private BetterDictionary<Cameras, List<LocalizedString>> currentSceneDictionary;
        private BetterDictionary<Cameras, bool> currentTutorialPart;
        private LocalizedString currentString;
        private int currentPart;

        private void OnEnable()
        {
            Message<CameraChangedEvent>.Add(OnCameraChangedEvent);
        }

        private void OnDisable()
        {
            Message<CameraChangedEvent>.Remove(OnCameraChangedEvent);
        }

        private void Start()
        {
            if (SaveManager.Instance.TutorialFinished)
            {
                HideTutorial();
            }
        }

        private void Update()
        {
            bool everythingFinished = true;
            foreach (BetterDictionary<Cameras, bool> tutorialPart in tutorialPartCompleted)
            {
                if (tutorialPart.Value == false)
                {
                    everythingFinished = false;
                    break;
                }
            }
            if (everythingFinished)
            {
                HideTutorial();
            }
        }

        private void OnCameraChangedEvent(CameraChangedEvent ctx)
        {
            if (!tutorialCompleteFinished)
            {
                BetterDictionary<Cameras, List<LocalizedString>> tdNew = FindTutorialTexts(ctx.NewCamera);
                BetterDictionary<Cameras, GameObject> newGhostPosition = FindGhostPosition(ctx.NewCamera);
                currentTutorialPart = FindTutorialPartFinished(ctx.NewCamera);
                if (currentTutorialPart != null)
                {
                    if (!currentTutorialPart.Value)
                    {
                        if (FindTutorialTexts(ctx.NewCamera) != null)
                        {
                            currentPart = 0;
                            currentSceneDictionary = tdNew;
                            DisplayText();
                            ghostGO.transform.DOMove(newGhostPosition.Value.transform.position, 1f);
                            ghostAnimator.SetBool("Hidden", false);
                        }
                        else
                        {
                            FindTutorialPartFinished(ctx.NewCamera).Value = true;
                            ghostAnimator.SetBool("Hidden", true);
                        }
                    }
                }
            }
        }

        private BetterDictionary<Cameras, List<LocalizedString>> FindTutorialTexts(Cameras sceneIn)
        {
            foreach (BetterDictionary<Cameras, List<LocalizedString>> td in tutorialTexts)
            {
                if (td.Key == sceneIn)
                {
                    return td;
                }
            }

            return null;
        }

        private BetterDictionary<Cameras, GameObject> FindGhostPosition(Cameras sceneIn)
        {
            foreach (BetterDictionary<Cameras, GameObject> td in ghostPositions)
            {
                if (td.Key == sceneIn)
                {
                    return td;
                }
            }

            return null;
        }
        
        private BetterDictionary<Cameras, bool> FindTutorialPartFinished(Cameras sceneIn)
        {
            foreach (BetterDictionary<Cameras, bool> td in tutorialPartCompleted)
            {
                if (td.Key == sceneIn)
                {
                    return td;
                }
            }

            return null;
        }

        public void NextDialogue()
        {
            if (!tutorialCompleteFinished)
            {
                DisplayText();
            }
            else
            {
                ghostAnimator.SetBool("Hidden", true);
            }
        }

        public void HideTutorial()
        {
            tutorialCompleteFinished = true;
            ghostAnimator.SetBool("Hidden", true);
            SaveManager.Instance.TutorialFinished = true;
        }

        private void DisplayText()
        {
            if (currentSceneDictionary != null)
            {
                if (!currentTutorialPart.Value)
                {
                    if (currentSceneDictionary.Value.Count > currentPart)
                    {
                        LocalizedString localizedString = currentSceneDictionary.Value[currentPart];
                        currentPart += 1;
                        tutorialTextfield.ChangeText(localizedString);
                    }
                    else
                    {
                        currentTutorialPart.Value = true;
                        ghostAnimator.SetBool("Hidden", true);
                    }
                }
            }
        }
    }
}