using UnityEngine;
using UnityEngine.Localization;

namespace PTWO_PR
{
    /// <summary>
    /// A scriptable object for all types of peeple
    /// </summary>
    [CreateAssetMenu(fileName = "New Peeple", menuName = "The Executioner/Peeple", order = 1)]
    public class Peeple : ScriptableObject
    {
        [Header("Visible Things")]
        [Tooltip("The language key for this peeple")] [SerializeField] private string peepleName;
        [Tooltip("A sprite for the hat front")] [SerializeField] private Sprite hairFront;
        [Tooltip("A sprite for the hat back")] [SerializeField] private Sprite hairBack;
        [Tooltip("A sprite for the pants")] [SerializeField] private Sprite pantsBack;
        [Tooltip("A sprite for the shoes")] [SerializeField] private Sprite pantsFront;

        public string PeepleName => peepleName;
        public Sprite HairFront => hairFront;
        public Sprite HairBack => hairBack;
        public Sprite PantsFront => pantsBack;
        public Sprite PantsBack => pantsFront;
    }
}