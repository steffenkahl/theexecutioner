using UnityEngine;
using UnityEngine.Localization;

namespace PTWO_PR
{
    /// <summary>
    /// A scriptable object for all types of cloth
    /// </summary>
    [CreateAssetMenu(fileName = "New Outfit", menuName = "The Executioner/Outfit", order = 1)]
    public class Outfit : ScriptableObject
    {
        [Header("Visible Things")]
        [Tooltip("The language key for this cloth")] [SerializeField] private LocalizedString clothNameKey;
        [Tooltip("The 2D sprite for this cloth")][SerializeField] private Sprite clothSprite;
        [Tooltip("The cost to buy this cloth")] [SerializeField] private int clothCost;
        
        [Header("Specific Things")]
        [Tooltip("Only for Shirts!")][SerializeField] private Sprite shirtArmFront;
        [Tooltip("Only for Shirts!")][SerializeField] private Sprite shirtArmBack;
        [Tooltip("Only for Legs!")][SerializeField] private Sprite pantsBack;
        
        [Header("Invisible Things")] 
        [Tooltip("If this cloth is already bought in new games")] [SerializeField] private bool boughtAtStart;
        [Tooltip("Where this Cloth is worn")] [SerializeField] private ClothPlaces clothPlace;

        public LocalizedString ClothNameKey => clothNameKey;
        public Sprite ClothSprite => clothSprite;
        public Sprite ShirtArmBack => shirtArmBack;
        public Sprite ShirtArmFront => shirtArmFront;
        public Sprite PantsBack => pantsBack;
        public int ClothCost => clothCost;
        public ClothPlaces ClothPlace => clothPlace;
        public bool BoughtAtStart => boughtAtStart;
    }
}