using UnityEngine;

namespace PTWO_PR
{
    /// <summary>
    /// A scriptable object for all types of cloth for the crowd
    /// </summary>
    [CreateAssetMenu(fileName = "New CrowdOutfit", menuName = "The Executioner/CrowdOutfit", order = 1)]
    public class CrowdOutfit : ScriptableObject
    {
        [Header("Visible Things")]
        [Tooltip("The 2D sprite for this cloth")][SerializeField] private Sprite clothSprite;
        
        [Header("Specific Things")]
        [Tooltip("Only for Shirts!")][SerializeField] private Sprite shirtArmLeft;
        [Tooltip("Only for Shirts!")][SerializeField] private Sprite shirtArmRight;
        [Tooltip("Only for Legs!")][SerializeField] private Sprite pantsBack;
        
        [Header("Invisible Things")] 
        [Tooltip("Where this Cloth is worn")] [SerializeField] private ClothPlaces clothPlace;

        public Sprite ClothSprite => clothSprite;
        public Sprite ShirtArmLeft => shirtArmLeft;
        public Sprite ShirtArmRight => shirtArmRight;
        public Sprite PantsBack => pantsBack;
        public ClothPlaces ClothPlace => clothPlace;
    }
}