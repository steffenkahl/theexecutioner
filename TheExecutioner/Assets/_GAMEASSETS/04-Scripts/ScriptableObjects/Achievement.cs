using UnityEngine;
using UnityEngine.Localization;

namespace PTWO_PR
{
    /// <summary>
    /// A scriptable object for all types of peeple
    /// </summary>
    [CreateAssetMenu(fileName = "New Achievement", menuName = "The Executioner/Achievement", order = 1)]
    public class Achievement : ScriptableObject
    {
        [SerializeField] private Sprite achievementImage;
        [SerializeField] private LocalizedString achievementTitle;
        [SerializeField] private LocalizedString achievementDescription;
        [SerializeField] private bool isSecret;
        [SerializeField] private Statistic requiredStatistic;
        [SerializeField] private int requiredAmount;

        public Sprite AchievementImage => achievementImage;
        public LocalizedString AchievementTitle => achievementTitle;
        public LocalizedString AchievementDescription => achievementDescription;
        public bool IsSecret => isSecret;
        public Statistic RequiredStatistic => requiredStatistic;
        public int RequiredAmount => requiredAmount;
    }
}