using UnityEngine;
using UnityEngine.Localization;

namespace PTWO_PR
{
    /// <summary>
    /// A scriptable object for all types of dialogues
    /// </summary>
    [CreateAssetMenu(fileName = "New Dialogue", menuName = "The Executioner/Dialogue", order = 1)]
    public class Dialogue : ScriptableObject
    {
        [Header("Visible Things")]
        [Tooltip("A sprite for the shirt")] [SerializeField] private Sprite shirt;
        [Tooltip("A sprite for the shirt arms")] [SerializeField] private Sprite shirtArms;
        [Tooltip("A optional sprite for the hair front")] [SerializeField] private Sprite optionalHairFront;
        [Tooltip("A optional sprite for the hair back")] [SerializeField] private Sprite optionalHairBack;
        [Tooltip("A optional sprite for the pants front")] [SerializeField] private Sprite optionalPantsFront;
        [Tooltip("A optional sprite for the pants back")] [SerializeField] private Sprite optionalPantsBack;
        [Tooltip("The language key for this dialogue")] [SerializeField] private LocalizedString dialogueKey;
        [Tooltip("The language key for a tip")] [SerializeField] private LocalizedString magicalTrowelTip;
        
        [Header("Stats")]
        [Tooltip("Damage")] [SerializeField] private float DMG;
        [Tooltip("Brutality")] [SerializeField] private float BRT;
        [Tooltip("Show")] [SerializeField] private float SHW;
        [SerializeField] private bool canBePardoned;

        public Sprite Shirt => shirt;
        public Sprite ShirtArms => shirtArms;
        public Sprite OptionalHairBack => optionalHairBack;
        public Sprite OptionalHairFront => optionalHairFront;
        public Sprite OptionalPantsBack => optionalPantsBack;
        public Sprite OptionalPantsFront => optionalPantsFront;

        public LocalizedString DialogueKey => dialogueKey;
        public LocalizedString MagicalTrowelTip => magicalTrowelTip;
        public float Dmg => DMG;
        public float Brt => BRT;
        public float Shw => SHW;
        public bool CanBePardoned => canBePardoned;
    }
}