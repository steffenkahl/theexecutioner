using UnityEngine;
using UnityEngine.Localization;

namespace PTWO_PR
{
    /// <summary>
    /// A scriptable object for all types of weapons
    /// </summary>
    [CreateAssetMenu(fileName = "New Weapon", menuName = "The Executioner/Weapon", order = 1)]
    public class Weapon : ScriptableObject
    {
        [Header("Visible Things")]
        [Tooltip("The language key for this weapon")] [SerializeField] private LocalizedString weaponNameKey;
        [Tooltip("The 2D sprite for this weapon")][SerializeField] private Sprite weaponSprite;
        [Tooltip("The cost to buy this weapon")] [SerializeField] private int weaponCost;

        [Header("Invisible Things")] 
        [Tooltip("A prefab for this weapon")] [SerializeField] private GameObject weaponPrefab;
        [Tooltip("The Scene used for this weapon")] [SerializeField] private Cameras usedScene;
        [Tooltip("The type of weapon (for animation)")] [SerializeField] private WeaponTypes weaponType;
        [Tooltip("If this weapon is already bought in new games")] [SerializeField] private bool boughtAtStart;
        
        [Header("Stats")]
        [Tooltip("Damage")] [SerializeField] private float DMG;
        [Tooltip("Brutality")] [SerializeField] private float BRT;
        [Tooltip("Show")] [SerializeField] private float SHW;

        public LocalizedString WeaponNameKey => weaponNameKey;
        public Sprite WeaponSprite => weaponSprite;
        public int WeaponCost => weaponCost;
        public GameObject WeaponPrefab => weaponPrefab;
        public Cameras UsedScene => usedScene;
        public WeaponTypes WeaponType => weaponType;
        public bool BoughtAtStart => boughtAtStart;
        public float Dmg => DMG;
        public float Brt => BRT;
        public float Shw => SHW;
    }
}