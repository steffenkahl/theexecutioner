﻿using System;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class ChangeCamera : MonoBehaviour
    {
        public void OnChangeCamera(String newCamera)
        {
            Cameras newCameraEnum;

            switch (newCamera)
            {
                default:
                    newCameraEnum = Cameras.NONE;
                    break;
                case "MENU":
                    newCameraEnum = Cameras.MENU;
                    break;
                case "CREDITS":
                    newCameraEnum = Cameras.CREDITS;
                    break;
                case "SETTINGS":
                    newCameraEnum = Cameras.SETTINGS;
                    break;
                case "WEAPONS":
                    newCameraEnum = Cameras.WEAPONS;
                    break;
                case "KERKER":
                    newCameraEnum = Cameras.DUNGEON;
                    break;
                case "CLOTH":
                    newCameraEnum = Cameras.CLOTH;
                    break;
                case "MAINSTAGE":
                    newCameraEnum = Cameras.MAINSTAGE;
                    break;
                case "RIVER":
                    newCameraEnum = Cameras.RIVER;
                    break;
                case "WEAPONSHOP":
                    newCameraEnum = Cameras.WEAPONSHOP;
                    break;
                case "CLOTHSHOP":
                    newCameraEnum = Cameras.CLOTHSHOP;
                    break;
                case "ACHIEVEMENTS":
                    newCameraEnum = Cameras.ACHIEVEMENTS;
                    break;
            }

        Message.Raise(new ChangeCameraEvent(newCameraEnum));
        }
    }
}