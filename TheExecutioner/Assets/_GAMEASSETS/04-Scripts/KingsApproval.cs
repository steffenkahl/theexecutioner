using System;
using System.Collections;
using System.Collections.Generic;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class KingsApproval : MonoBehaviour
    {
        [SerializeField] private Animator speechbubbleAnimator;
        [SerializeField] private SpriteRenderer speechbubbleRenderer;
        [SerializeField] private Sprite goodBubbleSprite;
        [SerializeField] private Sprite mediumBubbleSprite;
        [SerializeField] private Sprite badBubbleSprite;
        [SerializeField] private int badVotings;
        [SerializeField] private int maxBadVotings;
        [SerializeField] private int distanceForGood;
        [SerializeField] private int distanceForMedium;

        private void OnEnable()
        {
            Message<StartedPardonEvent>.Add(OnStartedPardonEvent);
            Message<FinishedExecutionEvent>.Add(OnFinishedExecutionEvent);
            Message<ClearSaveEvent>.Add(OnClearSaveEvent);
            Message<FailureFinishedEvent>.Add(OnFailureFinishedEvent);
        }
        
        private void OnDisable()
        {
            Message<StartedPardonEvent>.Remove(OnStartedPardonEvent);
            Message<FinishedExecutionEvent>.Remove(OnFinishedExecutionEvent);
            Message<ClearSaveEvent>.Remove(OnClearSaveEvent);
            Message<FailureFinishedEvent>.Remove(OnFailureFinishedEvent);
        }

        private void OnStartedPardonEvent(StartedPardonEvent ctx)
        {
            speechbubbleRenderer.sprite = badBubbleSprite;
            speechbubbleAnimator.SetTrigger("ShowBubble"); //Display the Bubble
            badVotings += 1;
            CheckForFailure();
        }

        private void OnFinishedExecutionEvent(FinishedExecutionEvent ctx)
        {
            if (ctx.DistanceToExcellentChoice < distanceForGood)
            {
                speechbubbleRenderer.sprite = goodBubbleSprite;
            }
            else if (ctx.DistanceToExcellentChoice < distanceForMedium)
            {
                speechbubbleRenderer.sprite = mediumBubbleSprite;
            }
            else
            {
                speechbubbleRenderer.sprite = badBubbleSprite;
                badVotings += 1;
                CheckForFailure();
            }
            
            speechbubbleAnimator.SetTrigger("ShowBubble");
        }

        private void CheckForFailure()
        {
            if (badVotings > maxBadVotings)
            {
                Message.Raise(new FailureEvent());
                Message.Raise(new ChangeCameraEvent(Cameras.DUNGEON));
            }
            else
            {
                Message.Raise(new KingBadImpressionEvent(badVotings));
            }
        }

        private void OnClearSaveEvent(ClearSaveEvent ctx)
        {
            badVotings = 0;
            Message.Raise(new KingBadImpressionEvent(badVotings));
        }

        private void OnFailureFinishedEvent(FailureFinishedEvent ctx)
        {
            badVotings = 0;
            Message.Raise(new KingBadImpressionEvent(badVotings));
        }
    }
}