﻿namespace PTWO_PR
{
    public class Enums
    { }

    public enum WeaponTypes
    {
        NONE,
        HANDWEAPON,
        FIXTURE,
        WORLDEVENT,
        AUDIENCEACTION,
    }

    public enum ClothPlaces
    {
        NONE,
        HAT,
        SHIRT,
        PANTS,
        DECO
    }
    
    public enum Cameras
    {
        NONE,
        MENU,
        CREDITS,
        SETTINGS,
        DUNGEON,
        WEAPONS,
        CLOTH,
        MAINSTAGE,
        RIVER,
        WEAPONSHOP,
        CLOTHSHOP,
        ACHIEVEMENTS,
    }

    public enum Statistic
    {
        NONE,
        VICTIMSKILLED_LIFE,
        VICTIMSKILLED_TOTAL,
        VICTIMSPARDONED_LIFE,
        VICTIMSPARDONED_TOTAL,
        WEAPONSBOUGHT_LIFE,
        WEAPONSBOUGHT_TOTAL,
        MONEYEARNED_LIFE,
        MONEYEARNED_TOTAL,
        CLOTHESBOUGHT,
        FAILURES,
        STAGEFEVERBOUGHT,
        MAGICTROWELBOUGHT,
        STAGEFEVERACTIVATED,
        MAGICTROWELACTIVATED,
        MONEYSPEND,
    }
}