using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using PTWO_PR.Events;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitializeManager : MonoBehaviour
{
    [SerializeField] private GameObject introImage;
    [SerializeField] private SpriteRenderer logoSprite;
    [SerializeField] private TMP_Text text1Sprite;
    [SerializeField] private TMP_Text text2Sprite;
    [SerializeField] private string gameScene;
    [SerializeField] private float waitTime;
    [SerializeField] private float endScale;
    [SerializeField] private float delayTime;

    private AsyncOperation loadingSceneOperation;
    private bool animationPlayed;
    private Scene currentScene;
    
    // Start is called before the first frame update
    void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        LoadScene();
        introImage.transform.DOScale(endScale, waitTime).OnComplete(()=>
        {
            animationPlayed = true;
        });
        text1Sprite.DOFade(1f, waitTime / 2).SetDelay(delayTime);
        text2Sprite.DOFade(1f, waitTime / 2).SetDelay(delayTime);
        logoSprite.DOFade(1f, waitTime / 2).SetDelay(delayTime);
    }

    private void LoadScene()
    {
        loadingSceneOperation = SceneManager.LoadSceneAsync(gameScene);
        loadingSceneOperation.allowSceneActivation = false;
    }

    private void Update()
    {
        if (loadingSceneOperation.progress >= .9f && animationPlayed)
        {
            loadingSceneOperation.allowSceneActivation = true;
        }
    }
}
