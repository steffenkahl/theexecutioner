using System;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class ParticleSystemTrigger : MonoBehaviour
    {
        [SerializeField] private ParticleSystem[] bloodParticles;
        [SerializeField] private ParticleSystem[] butterflyParticles;

        private void OnEnable()
        {
            Message<ExecutionImpactEvent>.Add(OnExecutionImpactEvent);
            Message<FinishedExecutionEvent>.Add(OnFinishedExecutionEvent);
            foreach (ParticleSystem ps in bloodParticles)
            {
                ps.Stop();
            }
            foreach (ParticleSystem ps in butterflyParticles)
            {
                ps.Stop();
            }
        }
        
        private void OnDisable()
        {
            Message<ExecutionImpactEvent>.Remove(OnExecutionImpactEvent);
            Message<FinishedExecutionEvent>.Remove(OnFinishedExecutionEvent);
        }

        private void OnExecutionImpactEvent(ExecutionImpactEvent ctx)
        {
            if (SaveManager.Instance.GoreActivated)
            {
                foreach (ParticleSystem ps in bloodParticles)
                {
                    ps.Play();
                }
            }
            else
            {
                foreach (ParticleSystem ps in butterflyParticles)
                {
                    ps.Play();
                }
            }
        }

        private void OnFinishedExecutionEvent(FinishedExecutionEvent ctx)
        {
            foreach (ParticleSystem ps in bloodParticles)
            {
                ps.Stop();
            }
            foreach (ParticleSystem ps in butterflyParticles)
            {
                ps.Stop();
            }
        }
    }
}