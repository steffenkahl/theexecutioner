using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace PTWO_PR
{
    public class FlameFlickering : MonoBehaviour
    {
        [SerializeField] private float minIntensity;
        [SerializeField] private float maxIntensity;
        [SerializeField] private float smoothTime;
        private Light2D lightSource;
        private float lastIntensity;
        private float newIntensity;

        private void Start()
        {
            lightSource = GetComponent<Light2D>();
        }

        private void Update()
        {
            newIntensity = Random.Range(minIntensity, maxIntensity);
            lightSource.intensity = Mathf.SmoothDamp(lightSource.intensity, newIntensity, ref newIntensity, smoothTime);
        }
    }
}