using System;
using UnityEngine;

namespace PTWO_PR
{
    public class VolumeHandler : MonoBehaviour
    {
        [Range(0.1f, 1.5f)][SerializeField] private float volumeModifier = 1.0f;
        private AudioSource source;

        private void Start()
        {
            source = GetComponent<AudioSource>();
            source.volume = SaveManager.Instance.MusicVolume;
        }

        private void Update()
        {
            source.volume = SaveManager.Instance.MusicVolume * volumeModifier;
        }
    }
}