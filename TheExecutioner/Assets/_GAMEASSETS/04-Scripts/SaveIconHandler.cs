using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using PTWO_PR.Events;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class SaveIconHandler : MonoBehaviour
    {
        [SerializeField] private Image saveIcon;
        [SerializeField] private float fadeDuration;
        [SerializeField] private float stayTime;

        private void OnEnable()
        {
            Message<SaveEvent>.Add(OnSaveEvent);
        }
        
        private void OnDisable()
        {
            Message<SaveEvent>.Remove(OnSaveEvent);
        }

        private void OnSaveEvent(SaveEvent ctx)
        {
            Sequence saveAnimation = DOTween.Sequence();
            saveAnimation.Append(saveIcon.DOFade(1f, fadeDuration));
            saveAnimation.Append(saveIcon.DOFade(0.5f, stayTime * 0.25f));
            saveAnimation.Append(saveIcon.DOFade(1f, stayTime * 0.25f));
            saveAnimation.Append(saveIcon.DOFade(0.5f, stayTime * 0.25f));
            saveAnimation.Append(saveIcon.DOFade(1f, stayTime * 0.25f));
            saveAnimation.Append(saveIcon.DOFade(0f, fadeDuration));
        }
    }
}
