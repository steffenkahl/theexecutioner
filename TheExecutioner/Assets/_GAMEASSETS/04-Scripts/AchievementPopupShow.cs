using System;
using System.Collections;
using System.Collections.Generic;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class AchievementPopupShow : MonoBehaviour
    {
        [SerializeField] private GameObject popupPrefab;

        private void OnEnable()
        {
            Message<EarnAchievementEvent>.Add(OnEarnAchievementEvent);
        }
        
        private void OnDisable()
        {
            Message<EarnAchievementEvent>.Remove(OnEarnAchievementEvent);
        }

        private void OnEarnAchievementEvent(EarnAchievementEvent ctx)
        {
            AchievementPopup popup = Instantiate(popupPrefab, transform).GetComponent<AchievementPopup>();
            Achievement ach = ctx.Achievement;
            int achievementCount = GameObject.FindGameObjectsWithTag(GameTags.ACHIEVEMENTPOPUP).Length;
            popup.SetAchievement(ach.AchievementImage, ach.AchievementTitle, ach.AchievementDescription, achievementCount);
        }
    }
}