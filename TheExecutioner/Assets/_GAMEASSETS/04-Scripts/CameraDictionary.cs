﻿using System;
using Cinemachine;
using Unity.IO.LowLevel.Unsafe;
using UnityEngine;

namespace PTWO_PR
{
    //Used extra Class for this Dictionary to see it in inspector
    [Serializable]
    public class CameraDictionary
    {
        [SerializeField] private Cameras key;
        [SerializeField] private CinemachineVirtualCamera value;

        public Cameras GetKey()
        {
            return key;
        }

        public CinemachineVirtualCamera GetValue()
        {
            return value;
        }
    }
}