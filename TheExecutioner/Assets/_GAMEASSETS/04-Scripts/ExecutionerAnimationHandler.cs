using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class ExecutionerAnimationHandler : MonoBehaviour
    {
        [SerializeField] private Animator executionAnim;
        [SerializeField] private CinemachineImpulseSource impulsSource;

        [SerializeField] private VictimCustomizer victimCustomizer;
        
        
        private void OnEnable()
        {
            Message<StartedExecutionEvent>.Add(OnStartedExecutionEvent);
            Message<StartedPardonEvent>.Add(OnStartedPardonEvent);
            executionAnim = GetComponent<Animator>();
        }
        
        private void OnDisable()
        {
            Message<StartedExecutionEvent>.Remove(OnStartedExecutionEvent);
            Message<StartedPardonEvent>.Remove(OnStartedPardonEvent);
        }

        private void OnStartedExecutionEvent(StartedExecutionEvent ctx)
        {
            //CHANGE THIS FOR DIFFERENT WEAPONTYPES!
            executionAnim.SetTrigger("kill");
        }
        
        private void OnStartedPardonEvent(StartedPardonEvent ctx)
        {
            //CHANGE THIS FOR DIFFERENT WEAPONTYPES!
            executionAnim.SetTrigger("pardon");
        }

        public void OnExecutionFinished()
        {
            GameManager.instance.CompareStats();
            Message.Raise(new LoadNewVictimEvent());
            Message.Raise(new IncreaseStatisticEvent(Statistic.VICTIMSKILLED_LIFE, 1));
            Message.Raise(new IncreaseStatisticEvent(Statistic.VICTIMSKILLED_TOTAL, 1));
        }

        public void OnPardoned()
        {
            GameManager.instance.PardonedMoney();
            Message.Raise(new LoadNewVictimEvent());
            Message.Raise(new IncreaseStatisticEvent(Statistic.VICTIMSPARDONED_LIFE, 1));
            Message.Raise(new IncreaseStatisticEvent(Statistic.VICTIMSPARDONED_TOTAL, 1));
        }

        public void OnImpact()
        {
            impulsSource.GenerateImpulse();
            Message.Raise(new ExecutionImpactEvent());
        }

        public void UpdateVictim()
        {
            if (victimCustomizer != null)
            {
                victimCustomizer.ChangeOutfit();
            }
        }
    }
}