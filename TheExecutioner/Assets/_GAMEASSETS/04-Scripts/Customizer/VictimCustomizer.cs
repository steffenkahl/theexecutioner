using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class VictimCustomizer : MonoBehaviour
    {
        [SerializeField] private bool reloadAfterVictimListen;
        [SerializeField] private SpriteRenderer hairFront;
        [SerializeField] private SpriteRenderer hairBack;
        [SerializeField] private SpriteRenderer shirt;
        [SerializeField] private SpriteRenderer shirtArms;
        [SerializeField] private SpriteRenderer pantsFront;
        [SerializeField] private SpriteRenderer pantsBack;

        private void OnEnable()
        {
            Message<VictimListenedEvent>.Add(OnVictimListenedEvent);
            Message<NewVictimLoadedEvent>.Add(OnNewVictimLoadedEvent);
        }
        
        private void OnDisable()
        {
            Message<VictimListenedEvent>.Remove(OnVictimListenedEvent);
            Message<NewVictimLoadedEvent>.Remove(OnNewVictimLoadedEvent);
        }

        private void OnVictimListenedEvent(VictimListenedEvent ctx)
        {
                ChangeOutfit();
        }
        
        private void OnNewVictimLoadedEvent(NewVictimLoadedEvent ctx)
        {
            if(!reloadAfterVictimListen)
                ChangeOutfit();
        }

        public void ChangeOutfit()
        {
            Peeple victim = VictimManager.Instance.ChosenPeep;
            Dialogue dialogue = VictimManager.Instance.ChosenDialogue;
            
            hairFront.sprite = victim.HairFront;
            hairBack.sprite = victim.HairBack;
            shirt.sprite = dialogue.Shirt;
            shirtArms.sprite = dialogue.ShirtArms;
            pantsFront.sprite = victim.PantsFront;
            pantsBack.sprite = victim.PantsBack;

            if (dialogue.OptionalHairBack && dialogue.OptionalHairFront)
            {
                hairBack.sprite = dialogue.OptionalHairBack;
                hairFront.sprite = dialogue.OptionalHairFront;
            }
            
            if (dialogue.OptionalPantsBack && dialogue.OptionalPantsFront)
            {
                pantsBack.sprite = dialogue.OptionalPantsBack;
                pantsFront.sprite = dialogue.OptionalPantsFront;
            }
        }
    }
}