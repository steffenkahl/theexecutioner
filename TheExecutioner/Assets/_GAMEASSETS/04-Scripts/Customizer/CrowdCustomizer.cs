using DG.Tweening;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    //Customizer for all Crowd People
    public class CrowdCustomizer : MonoBehaviour
    {
        //References to different body parts:
        [SerializeField] private SpriteRenderer hair;
        private Sprite hairSprite;
        [SerializeField] private SpriteRenderer shirt;
        private Sprite shirtSprite;
        [SerializeField] private SpriteRenderer shirtLeft;
        private Sprite shirtLeftSprite;
        [SerializeField] private SpriteRenderer shirtRight;
        private Sprite shirtRightSprite;
        [SerializeField] private SpriteRenderer pantsTop;
        private Sprite pantsTopSprite;
        [SerializeField] private SpriteRenderer pantsBottom;
        private Sprite pantsBottomSprite;
        
        [SerializeField] private Animator anim; //A reference to the animator to enable booing and cheering
        [SerializeField] private Sprite emptySprite; //An empty sprite to enable the stage fever effect
        private float destinationX; //The destination on the x-Axis that this peep should go to
        private MagicManager magicManager; //A reference to the magicManager to enable the stage fever effect

        private void Start()
        {
            magicManager = GameObject.FindWithTag(GameTags.MAGICMANAGER).GetComponent<MagicManager>(); //Find the magicManager
        }

        /// <summary>
        /// The target x-position for this crowd-peep
        /// </summary>
        public float DestinationX
        {
            get => destinationX;
            set => destinationX = value;
        }

        private void Update()
        {
            if (magicManager.StageFeverActive)
            {
                shirt.sprite = emptySprite;
                shirtLeft.sprite = emptySprite;
                shirtRight.sprite = emptySprite;
                pantsTop.sprite = emptySprite;
                pantsBottom.sprite = emptySprite;
            }
            else
            {
                hair.sprite = hairSprite;
                shirt.sprite = shirtSprite;
                shirtLeft.sprite = shirtLeftSprite;
                shirtRight.sprite = shirtRightSprite;
                pantsTop.sprite = pantsTopSprite;
                pantsBottom.sprite = pantsBottomSprite;
            }
        }

        private void OnEnable()
        {
            anim = GetComponent<Animator>();
            CustomizePeep();
            Message<CrowdCheerEvent>.Add(OnCrowdCheerEvent);
            Message<CrowdBooEvent>.Add(OnCrowdBooEvent);

            anim.speed = Random.Range(0.7f, 1.3f); //Randomize the animation speed a little bit
        }

        private void OnDisable()
        {
            Message<CrowdCheerEvent>.Remove(OnCrowdCheerEvent);
            Message<CrowdBooEvent>.Remove(OnCrowdBooEvent);
        }

        /// <summary>
        /// Customizes the Peep with cloth and positions
        /// </summary>
        private void CustomizePeep()
        {
            hairSprite = CrowdManager.Instance.GetRandomClothes(ClothPlaces.HAT).ClothSprite;
            CrowdOutfit shirtOutfit = CrowdManager.Instance.GetRandomClothes(ClothPlaces.SHIRT);
            shirtSprite = shirtOutfit.ClothSprite;
            shirtLeftSprite = shirtOutfit.ShirtArmLeft;
            shirtRightSprite = shirtOutfit.ShirtArmRight;
            CrowdOutfit pantsOutfit = CrowdManager.Instance.GetRandomClothes(ClothPlaces.PANTS);
            pantsTopSprite = pantsOutfit.ClothSprite;
            pantsBottomSprite = pantsOutfit.PantsBack;

            transform.DOMoveZ((transform.position.y) + 50, 0.1f);
        }

        /// <summary>
        /// Gets called whenever this crowd-peep should be able to cheer
        /// </summary>
        private void OnCrowdCheerEvent(CrowdCheerEvent ctx)
        {
            bool shouldCheer = Random.Range(0, 101 - ctx.PlausabilityToCheer) < 1;

            if (shouldCheer)
            {
                anim.SetTrigger("cheer");
            }
        }
        
        /// <summary>
        /// Gets called whenever this crowd-peep should be able to boo
        /// </summary>
        private void OnCrowdBooEvent(CrowdBooEvent ctx)
        {
            bool shouldBoo = Random.Range(0, 101 + ctx.PlausabilityToBoo) < 1;

            if (shouldBoo)
            {
                anim.SetTrigger("boo");
            }
        }

        /// <summary>
        /// Moves the crowd-peep off of the active stage
        /// </summary>
        public void MoveOffPlaza()
        {
            if (Random.Range(0, 1.5f) < 0.5f)
            {
                transform.DOMoveX(transform.position.x + 400, 4f).OnComplete(() => { Destroy(this.gameObject); });
            }
            else
            {
                transform.DOMoveX(transform.position.x - 400, 4f).OnComplete(() => { Destroy(this.gameObject); });
            }
        }

        /// <summary>
        /// Moves the crowd-peep on the active stage
        /// </summary>
        public void MoveOnPlaza()
        {
            transform.DOMoveX(destinationX, 4f);
        }
    }
}
