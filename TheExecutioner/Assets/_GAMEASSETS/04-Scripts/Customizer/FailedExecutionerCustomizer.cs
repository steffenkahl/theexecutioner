using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class FailedExecutionerCustomizer : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer hat;
        [SerializeField] private GameObject holder;

        private void OnEnable()
        {
            Message<FailureActivatedEvent>.Add(OnFailureActivatedEvent);
        }
        
        private void OnDisable()
        {
            Message<FailureActivatedEvent>.Remove(OnFailureActivatedEvent);
        }

        private void OnFailureActivatedEvent(FailureActivatedEvent ctx)
        {
            ChangeOutfit(ctx.LastHat.ClothSprite);
            holder.SetActive(true);
        }

        private void ChangeOutfit(Sprite hatSprite)
        {
            hat.sprite = hatSprite;
            Debug.Log("Hatsprite should be " + OutfitManager.Instance.ChosenHat.ClothSprite);
        }
    }
}