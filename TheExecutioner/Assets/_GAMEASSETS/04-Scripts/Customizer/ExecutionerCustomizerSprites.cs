using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class ExecutionerCustomizerSprites : MonoBehaviour
    {
        [SerializeField] private bool hasWeapon;
        [SerializeField] private GameObject weaponHolderHandheld;
        [SerializeField] private SpriteRenderer handheldWeapon;
        [SerializeField] private SpriteRenderer hat;
        [SerializeField] private SpriteRenderer shirt;
        [SerializeField] private SpriteRenderer shirtFront;
        [SerializeField] private SpriteRenderer shirtBack;
        [SerializeField] private SpriteRenderer pantsBack;
        [SerializeField] private SpriteRenderer pantsFront;
        [SerializeField] private SpriteRenderer deco;

        private void OnEnable()
        {
            Message<OutfitChangedEvent>.Add(OnOutfitChangedEvent);
            Message<CameraChangedEvent>.Add(OnCameraChangedEvent);
        }
        
        private void OnDisable()
        {
            Message<OutfitChangedEvent>.Remove(OnOutfitChangedEvent);
            Message<CameraChangedEvent>.Remove(OnCameraChangedEvent);
        }

        private void OnCameraChangedEvent(CameraChangedEvent ctx)
        {
            ChangeOutfit();
        }

        private void OnOutfitChangedEvent(OutfitChangedEvent ctx)
        {
            ChangeOutfit();
        }

        private void ChangeOutfit()
        {
            Weapon chosenWeapon = WeaponManager.Instance.ChosenWeapon;
            if (chosenWeapon != null)
            {
                if (chosenWeapon.WeaponType == WeaponTypes.HANDWEAPON)
                {
                    handheldWeapon.sprite = chosenWeapon.WeaponSprite;
                    weaponHolderHandheld.SetActive(hasWeapon);
                }
                else
                {
                    weaponHolderHandheld.SetActive(false);
                }
            }

            hat.sprite = OutfitManager.Instance.ChosenHat.ClothSprite;
            shirt.sprite = OutfitManager.Instance.ChosenShirt.ClothSprite;
            pantsFront.sprite = OutfitManager.Instance.ChosenLeg.ClothSprite;
            deco.sprite = OutfitManager.Instance.ChosenDeco.ClothSprite;

            if (OutfitManager.Instance.ChosenShirt.ShirtArmBack)
            {
                shirtBack.sprite = OutfitManager.Instance.ChosenShirt.ShirtArmBack;
            }

            if (OutfitManager.Instance.ChosenShirt.ShirtArmFront)
            {
                shirtFront.sprite = OutfitManager.Instance.ChosenShirt.ShirtArmFront;
            }

            if (OutfitManager.Instance.ChosenLeg.PantsBack)
            {
                pantsBack.sprite = OutfitManager.Instance.ChosenLeg.PantsBack;
            }
        }
    }
}