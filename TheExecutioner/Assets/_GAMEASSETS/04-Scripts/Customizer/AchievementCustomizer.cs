﻿using PTWO_PR.Events;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    //Customizer for Achievementitems in the Achievementlist
    public class AchievementCustomizer : MonoBehaviour
    {
        [SerializeField] private Image achImage; //A reference to the achievement icon
        [SerializeField] private Sprite emptySprite; //An empty sprite that gets shown if the player doesnt have an achievement
        [SerializeField] private LocalizeString achTitle; //A reference to the LocalizeString for the title of the game
        [SerializeField] private LocalizeString achDescription; //A reference to the LocalizeString for the description of the game
        [SerializeField] private Color colorCollected; //The color an achievement should have if the player earned it
        [SerializeField] private Color colorNotCollected; //The color an achievement should have if the player has not collected it yet
        [SerializeField] private bool isCollected; //A simple bool to show in the inspector if a achievement was collected
        [SerializeField] private Achievement achItem; //A reference to the Achievement-ScriptableObject that has all the data for this achievement
        [SerializeField] private Image background; //A reference to the AchievementItem-Background-Image to color it in

        private void OnEnable()
        {
            Message<EarnAchievementEvent>.Add(OnEarnAchievementEvent);
            Message<ClearSaveEvent>.Add(OnClearSaveEvent);
        }
        
        private void OnDisable()
        {
            Message<EarnAchievementEvent>.Remove(OnEarnAchievementEvent);
            Message<ClearSaveEvent>.Remove(OnClearSaveEvent);
        }

        /// <summary>
        /// Sets the data that should be used by this AchievementItem
        /// </summary>
        public void SetAchievement(Achievement achItemIn)
        {
            achTitle.ChangeText(achItemIn.AchievementTitle);
            if (!achItemIn.IsSecret)
            {
                achDescription.ChangeText(achItemIn.AchievementDescription);
            }
            achItem = achItemIn;
            SetColors();
        }

        /// <summary>
        /// Gets called whenever a new achievement is earned
        /// </summary>
        private void OnEarnAchievementEvent(EarnAchievementEvent ctx)
        {
            if (ctx.Achievement == achItem)
            {
                isCollected = true;
            }
            SetColors();
        }

        /// <summary>
        /// Set the colors of this AchievementItem to state the collected state
        /// </summary>
        private void SetColors()
        {
            if (isCollected)
            {
                background.color = colorCollected;
                achImage.sprite = achItem.AchievementImage;
                achDescription.ChangeText(achItem.AchievementDescription);
            }
            else
            {
                achImage.sprite = emptySprite;
                background.color = colorNotCollected;
            }
        }

        private void OnClearSaveEvent(ClearSaveEvent ctx)
        {
            Destroy(gameObject);
        }
    }
}