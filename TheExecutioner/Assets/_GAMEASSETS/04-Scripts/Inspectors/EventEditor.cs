﻿#if UNITY_EDITOR
using PTWO_PR.Events;
using UnityEditor;
using UnityEngine;

namespace PTWO_PR.Inspectors
{
    public class EventEditor : EditorWindow
    {
        [MenuItem("Tools/TheExecutioner/EventEditor")]

        public static void  ShowWindow () {
            EditorWindow.GetWindow(typeof(EventEditor), false, "EventEditor");
        }
        
        public void OnGUI()
        {
            if (GUILayout.Button("Save Game"))
            {
                Message.Raise(new SaveEvent());
            }
            
            if (GUILayout.Button("Load Game"))
            {
                Message.Raise(new LoadEvent());
            }
            
            if (GUILayout.Button("Clear Save"))
            {
                Message.Raise(new ClearSaveEvent());
            }
            
            GUILayout.Space(10);
            
            if (GUILayout.Button("Increase Money"))
            {
                Message.Raise(new MoneyChangeEvent(+50));
            }
            
            if (GUILayout.Button("Decrease Money"))
            {
                Message.Raise(new MoneyChangeEvent(-50));
            }
            
            GUILayout.Space(10);
            
            if (GUILayout.Button("Load New Victim"))
            {
                Message.Raise(new LoadNewVictimEvent());
            }
            
            GUILayout.Space(10);
            
            if (GUILayout.Button("Show Achievement"))
            {
                Message.Raise(new EarnAchievementEvent(AchievementManager.Instance.Achievements[0]));
            }
            
            if (GUILayout.Button("Trigger Failure"))
            {
                Message.Raise(new FailureEvent());
            }
        }
    }
}
#endif