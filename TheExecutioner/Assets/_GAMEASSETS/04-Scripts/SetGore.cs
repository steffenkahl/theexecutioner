using System;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class SetGore : MonoBehaviour
    {
        [SerializeField] private Toggle toggle;

        private void Start()
        {
            toggle.isOn = SaveManager.Instance.GoreActivated;
        }

        public void OnToggleChange()
        {
            SaveManager.Instance.GoreActivated = toggle.isOn;
        }
    }
}