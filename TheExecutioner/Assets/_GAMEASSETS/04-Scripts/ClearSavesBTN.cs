﻿using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class ClearSavesBTN : MonoBehaviour
    {
        public void ClearSaves()
        {
            Message.Raise(new ClearSaveEvent());
        }
    }
}