﻿using UnityEngine;

namespace PTWO_PR
{
    //Expansion of the standard Dictionary to be visible in inspector
    [System.Serializable]
    public class BetterDictionary<TKey, TValue>
    {
        [field: SerializeField] public TKey Key { set; get;}
        [field: SerializeField] public TValue Value { set; get;}

        public BetterDictionary()
        {
        }

        public BetterDictionary(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }
}