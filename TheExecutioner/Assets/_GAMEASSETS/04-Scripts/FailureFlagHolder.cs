using System;
using PTWO_PR.Events;
using UnityEngine;

namespace PTWO_PR
{
    public class FailureFlagHolder : MonoBehaviour
    {
        [SerializeField] private Animator flagHolderAnimator;

        private void OnEnable()
        {
            Message<KingBadImpressionEvent>.Add(OnKingBadImpressionEvent);
        }

        private void OnDisable()
        {
            Message<KingBadImpressionEvent>.Remove(OnKingBadImpressionEvent);
        }

        private void OnKingBadImpressionEvent(KingBadImpressionEvent ctx)
        {
            flagHolderAnimator.SetInteger("Flags", ctx.BadImpressions);
        }
    }
}